CREATE DATABASE db_thuebao
USE db_thuebao 
DROP DATABASE db_thuebao


create TABLE tblLoaiThueBao(
iMaLoai INT IDENTITY NOT NULL,
sTenLoai nvarchar(40),
constraint PK_tblLoaiThueBao primary key (iMaLoai),
);


create table tblNhanVien (
iMaNV int IDENTITY NOT NULL,
sTenNV nvarchar(40),
bGioitinh BIT NOT NULL,
dNgaysinh DATETIME NOT NULL,
dNgayvaolam DATETIME NOT NULL,
constraint PK_tblNhanVien primary key (iMaNV),
);

SELECT * FROM dbo.tblNhanVien


CREATE TABLE tbl_Account(
	sUserName NVARCHAR(100) PRIMARY KEY NOT NULL,
	sPassword NVARCHAR(100) NOT NULL,
	iID_NV INT,
	bLoaiAcc BIT NOT NULL, -- 0 admin	1--nv
	constraint FK_tblAccount_tblNV foreign key (iID_NV) references tblNhanVien(iMaNV),
)

create table tblThueBao (
iMaThueBao INT IDENTITY NOT NULL,
iMaLoai int NOT NULL,
sTenThueBao nvarchar(50),
bGioitinh BIT,
sSDT char(14),
iTuoi int,
sCMND char(20),
dNgayDangKy DATETIME,
constraint PK_tblThueBao primary key (iMaThueBao)
);
ALTER TABLE dbo.tblThueBao ADD CONSTRAINT FK_maloaitb FOREIGN KEY (iMaLoai) REFERENCES dbo.tblLoaiThueBao(iMaLoai);


create table tblHoaDon (
iMaHD INT IDENTITY NOT NULL,
iMaThueBao int NOT NULL,
iMaNV int NOT NULL,
fTongTien float,
dNgayLap DATETIME,
fThoiGianGoi INT,
iTinNhanGui INT,
constraint PK_tblHoaDon primary key (iMaHD),
constraint FK_tblHoaDon_tblNhanVien foreign key (iMaNV) references tblNhanVien(iMaNV),
constraint FK_tblHoaDon_tblThueBao foreign key (iMaThueBao) references tblThueBao(iMaThueBao)
);



CREATE PROC sp_dangnhap
@sUsername NVARCHAR(100),
@Password NVARCHAR(100)
AS
BEGIN
	SELECT * FROM dbo.tbl_Account WHERE dbo.tbl_Account.sUserName = @sUsername AND dbo.tbl_Account.sPassword = @Password
	END
	INSERT INTO dbo.tblNhanVien
	(
	    sTenNV,
	    bGioitinh,
	    dNgaysinh,
	    dNgayvaolam
	)
	VALUES
	(            -- iMaNV - int
	    N'A',       -- sTenNV - nvarchar(40)
	    1,      -- bGioitinh - bit
	    '04-02-1999 00:00:00AM', -- dNgaysinh - datetime
	    '12-03-2018 15:00:00PM'  -- dNgayvaolam - datetime
	    )

		-----
CREATE PROC sp_login(
@username NVARCHAR(100),@password NVARCHAR(100)
)
AS
BEGIN
	SELECT dbo.tbl_Account.iID_NV, dbo.tbl_Account.sUserName, dbo.tbl_Account.sPassword, dbo.tbl_Account.bLoaiAcc FROM dbo.tbl_Account
	WHERE dbo.tbl_Account.sUserName = @username AND	dbo.tbl_Account.sPassword = @password
	END
EXECUTE sp_login N'Su', N'db7a035adb59265e0f89b892a0e623bb'
drop proc sp_login

ALTER PROC sp_getdataNV
AS
BEGIN	
SELECT * FROM dbo.tblNhanVien WHERE bTrangthai = 0;
END

EXECUTE sp_getdataNV
---
CREATE PROC sp_thuebaotratruoc
AS
BEGIN	
	SELECT dbo.tblThueBao.*,dbo.tblHoaDon.fTongTien FROM dbo.tblThueBao,dbo.tblHoaDon
	WHERE  dbo.tblThueBao.iMaThueBao = dbo.tblHoaDon.iMaThueBao AND dbo.tblThueBao.iMaLoai= 2;
END
CREATE PROC sp_nghiviec(
@bTrangthai BIT, @iMaNV INT)
AS 
BEGIN
UPDATE dbo.tblNhanVien 
SET bTrangthai = @bTrangthai
WHERE iMaNV = @iMaNV
END
EXECUTE sp_nghiviec 0, 1
SELECT * FROM	 dbo.tblNhanVien
EXECUTE sp_thuebaotratruoc

CREATE PROC sp_thuebaotrasau
AS
BEGIN	
	SELECT dbo.tblThueBao.*,dbo.tblHoaDon.fTongTien FROM dbo.tblThueBao,dbo.tblHoaDon
	WHERE  dbo.tblThueBao.iMaThueBao = dbo.tblHoaDon.iMaThueBao AND dbo.tblThueBao.iMaLoai= 1;
END
EXECUTE sp_thuebaotrasau
ALTER PROC sp_thuebaotrasautruoc
AS
BEGIN	
	SELECT * FROM dbo.tblThueBao
	WHERE  dbo.tblThueBao.iMaLoai= 2 AND bTrangthai = 0;
END

	EXECUTE sp_thuebaotrasautruoc


CREATE PROC sp_hoadonthuebaotratruoc
AS
BEGIN
	SELECT dbo.tblHoaDon.*,dbo.tblThueBao.sTenThueBao,dbo.tblThueBao.bGioitinh,dbo.tblThueBao.sSDT,dbo.tblThueBao.iMaLoai FROM dbo.tblHoaDon,dbo.tblThueBao
	WHERE dbo.tblHoaDon.iMaThueBao = dbo.tblThueBao.iMaThueBao AND dbo.tblThueBao.iMaLoai = 2
END

	EXECUTE sp_hoadonthuebaotratruoc



CREATE PROC sp_hoadonthuebaotrasau
AS
BEGIN
	SELECT dbo.tblHoaDon.*,dbo.tblThueBao.sTenThueBao,dbo.tblThueBao.bGioitinh,dbo.tblThueBao.sSDT,dbo.tblThueBao.iMaLoai FROM dbo.tblHoaDon,dbo.tblThueBao
	WHERE dbo.tblHoaDon.iMaThueBao = dbo.tblThueBao.iMaThueBao AND dbo.tblThueBao.iMaLoai = 1
END

	EXECUTE sp_hoadonthuebaotrasau

	---INSERT
	CREATE PROC sp_themnv (
		 @sTenNV NVARCHAR(40), @bGioitinh BIT, @dNgaysinh DATETIME, @dNgayvaolam DATETIME
	)
	AS
	BEGIN
		INSERT INTO dbo.tblNhanVien
		(
		    sTenNV,
		    bGioitinh,
		    dNgaysinh,
		    dNgayvaolam
		)
		VALUES
		(  
		 @sTenNV , @bGioitinh, @dNgaysinh, @dNgayvaolam 
		)
	END

	CREATE PROC sp_themaccount(
	@sUsername NVARCHAR(100), @sPassword NVARCHAR(100), @iID_NV INT
	)
	AS
	BEGIN
		INSERT INTO dbo.tbl_Account
		(
		    sUserName,
		    sPassword,
		    iID_NV,
		    bLoaiAcc
		)
		VALUES
		(   
		@sUsername , @sPassword , @iID_NV , 1
		    )
	END


			SELECT * FROM dbo.tblNhanVien
			SELECT * FROM  dbo.tbl_Account
			EXECUTE sp_themaccount N'Linh2', N'12', 2
---
CREATE PROC sp_updatenv(
@iMaNV INT, @sTenNV NVARCHAR(40),@bGioitinh BIT, @dNgaysinh DATETIME, @dNgayvaolam DATETIME)
AS
BEGIN
	UPDATE dbo.tblNhanVien
	SET sTenNV = @sTenNV, bGioitinh = @bGioitinh, dNgaysinh = @dNgaysinh, dNgayvaolam = @dNgayvaolam
	WHERE iMaNV = @iMaNV
END


	CREATE PROC sp_themthuebaotratruoc(
	 @sTenthuebao NVARCHAR(50), @bGioitinh BIT, @sSDT CHAR(14),@iTuoi INT, @sCMND CHAR(20),@dNgaydangky DATE
	)
	AS
	BEGIN
		INSERT INTO dbo.tblThueBao
		(
		    iMaLoai,
		    sTenThueBao,
		    bGioitinh,
		    sSDT,
		    iTuoi,
		    sCMND,
		    dNgayDangKy
		)
		VALUES
		(   
			2, @sTenthuebao, @bGioitinh, @sSDT, @iTuoi, @sCMND,@dNgaydangky
		    )
		END

			EXECUTE sp_themthuebaotratruoc   N'Hoang Anh',0,'012312323',23,'0312312341','2018-02-03'

			
CREATE PROC sp_ngungdv(
@bTrangthai BIT, @iMathuebao INT)
AS 
BEGIN
UPDATE dbo.tblThueBao
SET bTrangthai = @bTrangthai
WHERE iMaThueBao = @iMathuebao
END
EXECUTE sp_ngungdv 0, 2
SELECT * FROM dbo.tblThueBao
	
CREATE PROC sp_thongketheongay(
 @dNgaycantke DATETIME, @dNgaybatdautke DATETIME, @iMaLoai INT)
AS
BEGIN
SELECT dbo.tblHoaDon.*,dbo.tblThueBao.sTenThueBao,dbo.tblThueBao.bGioitinh,dbo.tblThueBao.sSDT,dbo.tblThueBao.iMaLoai FROM dbo.tblHoaDon,dbo.tblThueBao
	WHERE dbo.tblHoaDon.iMaThueBao = dbo.tblThueBao.iMaThueBao AND dbo.tblThueBao.iMaLoai = @iMaLoai AND (dbo.tblHoaDon.dNgayLap BETWEEN @dNgaybatdautke AND @dNgaycantke)
END


EXECUTE sp_thongketheongay  '2019-04-20 00:00:00.000', '2012-03-03 00:00:00.000', 1
SELECT * FROM dbo.tblHoaDon 


CREATE PROC sp_tong (
@iMaLoai INT, @dNgaycantke DATETIME, @dNgaybatdautke DATETIME)
AS
BEGIN
    SELECT SUM(dbo.tblHoaDon.fTongTien) AS fTongtientatcathuebao
	FROM  dbo.tblHoaDon INNER JOIN dbo.tblThueBao ON tblHoaDon.iMaThueBao = tblThueBao.iMaThueBao
	WHERE  iMaLoai = @iMaLoai AND (dbo.tblHoaDon.dNgayLap BETWEEN  @dNgaybatdautke AND @dNgaycantke)
END
EXECUTE sp_tong 2 ,'2019-02-03 00:00:00.000', '2012-03-03 00:00:00.000'
SELECT * FROM dbo.tblHoaDon


CREATE PROC sp_getNvById																		
@id int																							
AS
BEGIN
SELECT  * FROM dbo.tblNhanVien, dbo.tbl_Account
WHERE dbo.tblNhanVien.iMaNV = @id AND dbo.tbl_Account.iID_NV = dbo.tblNhanVien.iMaNV
END

exec sp_getNvById 1

-------------------------------------------------------------------------------------------------
CREATE PROC sp_resetPassword																		
@id int, @pass varchar(50)																						
AS
BEGIN
	update dbo.tbl_Account
	set	tbl_Account.sPassword = @pass
	where tbl_Account.iID_NV = @id
END

exec sp_resetPassword 5

