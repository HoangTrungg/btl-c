﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DAO;
using WindowsFormsApp1.DTO;

namespace WindowsFormsApp1
{
    public partial class fthuebaongungdichvu : Form
    {
        public fthuebaongungdichvu()
        {
            InitializeComponent();
            loadlistviewtrasau();
        }
        List<TrasaunhanvienDTO> list_trasau;
        private void loadlistviewtrasau()
        {
            lv_tbtrasauhuydichvu.Items.Clear();
            list_trasau = TrasauDAO.Instance.getdatathuebaongunfdv(null, null);
            ListViewItem item;
            foreach (TrasaunhanvienDTO trasau in list_trasau)
            {
                item = new ListViewItem();
                item.Text = trasau.IMathuebao.ToString();
                item.SubItems.Add(trasau.IMaloai.ToString());
                item.SubItems.Add(trasau.STenthuebao.ToString());
                item.SubItems.Add(trasau.bGioitinh == true ? "Nu" : "Nam");
                item.SubItems.Add(trasau.SSDT.ToString());
                item.SubItems.Add(trasau.ITuoi.ToString());
                item.SubItems.Add(trasau.SCMND.ToString());
                item.SubItems.Add(trasau.DNgaydangky.ToString("dd/MM/yyyy"));
                lv_tbtrasauhuydichvu.Items.Add(item);
            }
        }

        List<TratruocDTO> list_tratruoc;
        private void loadlistviewtratruoc()
        {
            lv_tbtrasauhuydichvu.Items.Clear();
            list_tratruoc = TratruocDAO.Instance.getdatanvtratruochuydv(null, null);
            ListViewItem item;
            foreach (TratruocDTO trasau in list_tratruoc)
            {
                item = new ListViewItem();
                item.Text = trasau.IMathuebao.ToString();
                item.SubItems.Add(trasau.IMaloai.ToString());
                item.SubItems.Add(trasau.STenthuebao.ToString());
                item.SubItems.Add(trasau.bGioitinh == true ? "Nu" : "Nam");
                item.SubItems.Add(trasau.SSDT.ToString());
                item.SubItems.Add(trasau.ITuoi.ToString());
                item.SubItems.Add(trasau.SCMND.ToString());
                item.SubItems.Add(trasau.DNgaydangky.ToString("dd/MM/yyyy"));
                lv_tbtrasauhuydichvu.Items.Add(item);
            }
        }
        private void Lv_tbtrasauhuydichvu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lv_tbtrasauhuydichvu.SelectedItems.Count <= 0)
                return;
            txt_mathuebao.Text = lv_tbtrasauhuydichvu.SelectedItems[0].Text.ToString();
            txt_loaithuebao.Text = lv_tbtrasauhuydichvu.SelectedItems[0].SubItems[1].Text.ToString();
            txt_tenthuebao.Text = lv_tbtrasauhuydichvu.SelectedItems[0].SubItems[2].Text.ToString();
            txt_sdt.Text = lv_tbtrasauhuydichvu.SelectedItems[0].SubItems[4].Text.ToString();

            if (lv_tbtrasauhuydichvu.SelectedItems[0].SubItems[3].Text.ToString().Equals("Nu"))
            {
                rd_nu.Checked = true;
            }
            else
                rd_Nam.Checked = true;

            txt_tuoi.Text = lv_tbtrasauhuydichvu.SelectedItems[0].SubItems[5].Text.ToString();
            txt_CMND.Text = lv_tbtrasauhuydichvu.SelectedItems[0].SubItems[6].Text.ToString();
            dt_pickngaydangky.Value = DateTime.ParseExact(lv_tbtrasauhuydichvu.SelectedItems[0].SubItems[7].Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

            //dt_pickngaydangky.Value = DateTime.ParseExact(lv_tbtrasauhuydichvu.SelectedItems[0].SubItems[7].Text, "dd/MM/yyyy", new CultureInfo("vi-VN"));

        }

        private void Txt_sdt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void Txt_CMND_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void Btn_thuebaotrasau_Click(object sender, EventArgs e)
        {
            loadlistviewtrasau();
            btn_thuebaotrasau.Enabled = false;
            btn_thuebaotratruoc.Enabled = true;
            lb_thuebao.Text = "Thuê bao trả sau";
            txt_mathuebao.Text = "";
            txt_loaithuebao.Text = "";
            txt_tenthuebao.Text = "";
            txt_sdt.Text = "";
            rd_Nam.Checked = true;


            txt_tuoi.Text = "";
            txt_CMND.Text = "";
            dt_pickngaydangky.Value = DateTime.Now;
        }

        private void Btn_thuebaotratruoc_Click(object sender, EventArgs e)
        {
            loadlistviewtratruoc();
            btn_thuebaotrasau.Enabled = true;
            btn_thuebaotratruoc.Enabled = false;
            lb_thuebao.Text = "Thuê bao trả trước";
            txt_mathuebao.Text = "";
            txt_loaithuebao.Text = "";
            txt_tenthuebao.Text = "";
            txt_sdt.Text = "";
            rd_Nam.Checked = true;
            txt_tuoi.Text = "";
            txt_CMND.Text = "";
            dt_pickngaydangky.Value = DateTime.Now;
        }

        private void Btn_capdv_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Cấp lại dịch vụ ?", "Cấp Lại", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
            {
                List<string> listpara = new List<string> { "@iMathuebao", "@bTrangthai" };
                int itrangthai = 0;
                int iMatb = int.Parse(txt_mathuebao.Text);
                object[] parameter = new object[] { iMatb, itrangthai };
                if (TrasauDAO.Instance.updatetrangthai(parameter, listpara))
                {
                    MessageBox.Show("Đã cấp quyền sử dụng dịch vụ đối với" + txt_tenthuebao.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Chưa thể cấp quyền dịch vụ của" + txt_tenthuebao.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                if (btn_thuebaotrasau.Enabled == false)
                {
                    loadlistviewtrasau();
                }
                else
                {
                    loadlistviewtratruoc();
                }
            }
            else
            {
                return;
            }

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
