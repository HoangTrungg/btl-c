﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class bum
    {
        private static string getpumhash(byte[] data)
        {
            StringBuilder sbuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sbuilder.Append(data[i].ToString("x2"));
            }
            return sbuilder.ToString();
        }
        public static string Hash(string data)
        {
            using (var pum = MD5.Create())
            {
                return getpumhash(pum.ComputeHash(Encoding.UTF8.GetBytes(data)));
            }
        }
    }
}
