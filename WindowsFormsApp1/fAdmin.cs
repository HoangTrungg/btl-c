﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using WindowsFormsApp1.DAO;
using WindowsFormsApp1.DTO;

namespace WindowsFormsApp1
{
    public partial class fAdmin : Form
    {
        public fAdmin()
        {
            InitializeComponent();
            LoadlistviewNV();
            loadlisttratruoc();
            loadlistviewtrasau();
            loadNvDetail();
        }
        List<NhanVienDTO> listnv;
        ErrorProvider errorProvider = new ErrorProvider();
        DataProvider dtp = new DataProvider();
        private Boolean flag = false;
        string tmpfile = Path.GetTempPath();
        private void LoadlistviewNV()
        {
            lv_nhanvien.Items.Clear();
            listnv = NhanvienDAO.Instance.getdata(null, null);
            ListViewItem item;
            foreach (NhanVienDTO nv in listnv)
            {
                item = new ListViewItem();
                item.Text = nv.IMaNV.ToString();
                item.SubItems.Add(nv.STenNV.ToString());
                item.SubItems.Add(nv.BGioitinh == true ? "Nữ" : "Nam");
                item.SubItems.Add(nv.DNgaysinh.ToString("dd/MM/yyyy"));
                item.SubItems.Add(nv.DNgayvaolam.ToString("dd/MM/yyyy"));
                lv_nhanvien.Items.Add(item);
            }
        }
        List<HoadonthuebaotratruocDTO> list_tratruoc;
        private void loadlisttratruoc()
        {
            lv_tratruoc.Items.Clear();
            list_tratruoc = HoadonthuebaotratruocDAO.Instance.getdata(null, null);
            ListViewItem item;
            foreach (HoadonthuebaotratruocDTO hdtratruoc in list_tratruoc)
            {
                //int iMaHD, int iMathuebao, int iMaNV,int iMaLoai, string sTenThueBao, bool bGioitinh,
                //string sSDT, float ftongtien, DateTime dNgaylap, int iThoigian, int iTinnhangui
                item = new ListViewItem();
                item.Text = hdtratruoc.IMaHD.ToString();
                item.SubItems.Add(hdtratruoc.IMathuebao.ToString());
                item.SubItems.Add(hdtratruoc.IMaNV.ToString());
                item.SubItems.Add(hdtratruoc.IMaLoai.ToString());
                item.SubItems.Add(hdtratruoc.STenThueBao.ToString());
                item.SubItems.Add(hdtratruoc.BGioitinh == true ? "Nữ" : "Nam");
                item.SubItems.Add(hdtratruoc.SSDT.ToString());
                item.SubItems.Add(hdtratruoc.FTongtien.ToString());
                item.SubItems.Add(hdtratruoc.DNgaylap.ToString("dd/MM/yyyy"));
                item.SubItems.Add(hdtratruoc.Ithoigian.ToString());
                item.SubItems.Add(hdtratruoc.ITinnhangui.ToString());
                lv_tratruoc.Items.Add(item);
            }
        }
        List<HoadonthuebaotrasauDTO> list_trasau;
        private void loadlistviewtrasau()
        {
            lv_tbtrasau.Items.Clear();
            list_trasau = HoadonthuebaotrasauDAO.Instance.getdata(null, null);
            ListViewItem item;
            foreach (HoadonthuebaotrasauDTO hdtrasau in list_trasau)
            {
                item = new ListViewItem();
                item.Text = hdtrasau.IMaHD.ToString();
                item.SubItems.Add(hdtrasau.IMathuebao.ToString());
                item.SubItems.Add(hdtrasau.IMaNV.ToString());
                item.SubItems.Add(hdtrasau.IMaLoai.ToString());
                item.SubItems.Add(hdtrasau.STenThueBao.ToString());
                item.SubItems.Add(hdtrasau.BGioitinh == true ? "Nữ" : "Nam");
                item.SubItems.Add(hdtrasau.SSDT.ToString());
                item.SubItems.Add(hdtrasau.FTongtien.ToString());
                item.SubItems.Add(hdtrasau.DNgaylap.ToString("dd/MM/yyyy"));
                item.SubItems.Add(hdtrasau.Ithoigian.ToString());
                item.SubItems.Add(hdtrasau.ITinnhangui.ToString());
                lv_tbtrasau.Items.Add(item);
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            fthemnv fthemnv = new fthemnv();
            fthemnv.ShowDialog();
            this.Show();
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            fTaotaikhoan ftaotk = new fTaotaikhoan();
            //this.Hide();
            ftaotk.ShowDialog();
            this.Show();
        }

        private void Lv_nhanvien_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lv_nhanvien.SelectedItems.Count <= 0)
                return;
            txt_manv.Text = lv_nhanvien.SelectedItems[0].Text.ToString();
            txt_tennv.Text = lv_nhanvien.SelectedItems[0].SubItems[1].Text.ToString();
            if (lv_nhanvien.SelectedItems[0].SubItems[2].Text.ToString().Equals("Nữ"))
            {
                rd_nu.Checked = true;
            }
            else
                rd_nam.Checked = true;
            //dt_pickngaysinh.Value = DateTime.ParseExact(lv_nhanvien.SelectedItems[0].SubItems[3].Text, "dd/MM/yyyy", new CultureInfo("vi-VN"));
            //dt_pickngayvaolam.Value = DateTime.ParseExact(lv_nhanvien.SelectedItems[0].SubItems[4].Text, "dd/MM/yyyy", new CultureInfo("vi-VN"));

            dt_pickngaysinh.Value = DateTime.ParseExact(lv_nhanvien.SelectedItems[0].SubItems[3].Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            dt_pickngayvaolam.Value = DateTime.ParseExact(lv_nhanvien.SelectedItems[0].SubItems[4].Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);


        }

        private void Button3_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Xác nhận thay đổi?", "Thay Đổi ?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
            {
                List<String> listpara = new List<string> { "@iMaNV", "@sTenNV", "@bGioitinh", "@dNgaysinh", "@dNgayvaolam" };
                int iMaNV = int.Parse(txt_manv.Text);
                string sTenNV = txt_tennv.Text;
                bool bGT = rd_nam.Checked ? false : true;
                DateTime dNgaysinh = Convert.ToDateTime(dt_pickngaysinh.Value.ToString());
                DateTime dNgayvaolam = Convert.ToDateTime(dt_pickngayvaolam.Value.ToString());
                object[] parameter = new object[] { iMaNV, sTenNV, bGT, dNgaysinh, dNgayvaolam };
                if (NhanvienDAO.Instance.update(parameter, listpara))
                {
                    MessageBox.Show("Sửa thành công !!");
                    LoadlistviewNV();
                }
                else
                {
                    MessageBox.Show("Loi!");
                }
            }
            else
            {
                MessageBox.Show("Hủy thao tác ?");
            }

        }

        private void passUpdate_Click(object sender, EventArgs e)
        {
            if (flag == false)
            {
                MessageBox.Show("Kiểm tra lại thông tin");
            }
            else
            {
                string oldPass = oldpass.Text;
                string newPass = newpass.Text;
                string id = "";
                using (StreamReader sr = File.OpenText(fDangnhap.tmpfile))
                {
                    id = sr.ReadLine();
                }
                string pass = bum.Hash(newPass);
                string opass = bum.Hash(oldPass);
                int result = dtp.ExcuteNoneQuery("sp_resetPassword", new object[] { id, pass, opass }, new List<string> { "@id", "@pass", "@oldpass" });
                if (result == 0)
                {
                    MessageBox.Show("Đổi Mật Khẩu Không Thành Công");
                }
                else
                {
                    MessageBox.Show("Đổi Mật Khẩu Thành Công, Chuyển Về Trang Đăng Nhập");
                    fDangnhap.DeleteTmpFile(fDangnhap.tmpfile);
                    this.Close();
                }
            }
        }

        private void repass_KeyUp(object sender, KeyEventArgs e)
        {
            if (repass.Text.Equals(newpass.Text) == false)
            {
                errorProvider.SetError(repass, "Mật Khẩu Không Trùng Khớp");
                flag = false;
                passUpdate.Enabled = false;
            }
            else
            {
                errorProvider.Clear();
                passUpdate.Enabled = true;
                flag = true;
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Thay đổi trạng thái của nhân viên?", "Thay Đổi ?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
            {
                int iTrangthai = 1;
                int iMaNV = int.Parse(txt_manv.Text);
                List<String> listpara = new List<string> { "@iMaNV", "@bTrangthai" };
                object[] parameter = new object[] { iMaNV, iTrangthai };
                if (NhanvienDAO.Instance.updatetrangthainghiviec(parameter, listpara))
                {
                    MessageBox.Show("Sửa thành công !!");
                    LoadlistviewNV();
                }
                else
                {
                    MessageBox.Show("Loi!");
                }
            }
            else
            {
                MessageBox.Show("Đã hủy thao tác");
                return;
            }

        }

        private void Btn_xem_Click(object sender, EventArgs e)
        {
            loadlisttratruoctheongay();
            loadtxtdoanhthu();
        }
        List<HoadonthuebaotratruocDTO> list_tratruoctheongay;
        private void loadlisttratruoctheongay()
        {
            int iMaloaitb = 2;
            List<string> listparameter = new List<string> { "@dNgaycantke", "@dNgaybatdautke", "@iMaLoai" };
            DateTime dt_batdau = Convert.ToDateTime(dt_picktu.Value.ToString());
            DateTime dt_den = Convert.ToDateTime(dt_pickden.Value.ToString());
            DateTime datenow = DateTime.Now;
            object[] parameter = new object[] { dt_den, dt_batdau, iMaloaitb };
            lv_tratruoc.Items.Clear();
            if (dt_batdau > dt_den || dt_batdau > datenow || dt_den > datenow)
            {
                MessageBox.Show("Xem lai moc thoi gian vua nhap!!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                list_tratruoctheongay = HoadonthuebaotratruocDAO.Instance.getdatatketheongay(parameter, listparameter);
                ListViewItem item;
                foreach (HoadonthuebaotratruocDTO hdtratruoc in list_tratruoctheongay)
                {
                    item = new ListViewItem();
                    item.Text = hdtratruoc.IMaHD.ToString();
                    item.SubItems.Add(hdtratruoc.IMathuebao.ToString());
                    item.SubItems.Add(hdtratruoc.IMaNV.ToString());
                    item.SubItems.Add(hdtratruoc.IMaLoai.ToString());
                    item.SubItems.Add(hdtratruoc.STenThueBao.ToString());
                    item.SubItems.Add(hdtratruoc.BGioitinh == true ? "Nữ" : "Nam");
                    item.SubItems.Add(hdtratruoc.SSDT.ToString());
                    item.SubItems.Add(hdtratruoc.FTongtien.ToString());
                    item.SubItems.Add(hdtratruoc.DNgaylap.ToString("dd/MM/yyyy"));
                    item.SubItems.Add(hdtratruoc.Ithoigian.ToString());
                    item.SubItems.Add(hdtratruoc.ITinnhangui.ToString());
                    lv_tratruoc.Items.Add(item);
                }
            }

        }
        private void loadtxtdoanhthu()
        {
            int iMaloaitb = 2;
            List<string> listparameter = new List<string> { "@iMaLoai", "@dNgaycantke", "@dNgaybatdautke" };

            DateTime dt_batdau = Convert.ToDateTime(dt_picktu.Value.ToString());
            DateTime dt_den = Convert.ToDateTime(dt_pickden.Value.ToString());
            object[] parameter = new object[] { iMaloaitb, dt_den, dt_batdau };
            string sum = HoadonthuebaotratruocDAO.Instance.gettong(parameter, listparameter);
            txt_tongtientratruoc.Text = sum;


        }

        private void Btn_thongke_Click(object sender, EventArgs e)
        {
            loadlisttrasautheongay();
            loadtxtdoanhthutrasau();
        }
        List<HoadonthuebaotrasauDTO> list_trasautheongay;
        private void loadlisttrasautheongay()
        {
            int iMaloaitb = 1;
            List<string> listparameter = new List<string> { "@dNgaycantke", "@dNgaybatdautke", "@iMaLoai" };
            DateTime dt_batdau = Convert.ToDateTime(dt_picktrasautu.Value.ToString());
            DateTime dt_den = Convert.ToDateTime(dt_picktrasauden.Value.ToString());
            DateTime datenow = DateTime.Now;
            object[] parameter = new object[] { dt_den, dt_batdau, iMaloaitb };
            lv_tbtrasau.Items.Clear();
            if (dt_batdau > dt_den || dt_batdau > datenow || dt_den > datenow)
            {
                MessageBox.Show("Xem lại mốc thời gian !!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                list_trasautheongay = HoadonthuebaotrasauDAO.Instance.getdatatketheongay(parameter, listparameter);
                ListViewItem item;
                if(list_trasautheongay.Count == 0)
                {
                    lbNoData.Visible = true;
                }
                else
                {
                    lbNoData.Visible = false;
                }
                MessageBox.Show(list_trasautheongay.Count.ToString());
                foreach (HoadonthuebaotrasauDTO hdtrasau in list_trasautheongay)
                {
                    item = new ListViewItem();
                    item.Text = hdtrasau.IMaHD.ToString();
                    item.SubItems.Add(hdtrasau.IMathuebao.ToString());
                    item.SubItems.Add(hdtrasau.IMaNV.ToString());
                    item.SubItems.Add(hdtrasau.IMaLoai.ToString());
                    item.SubItems.Add(hdtrasau.STenThueBao.ToString());
                    item.SubItems.Add(hdtrasau.BGioitinh == true ? "Nữ" : "Nam");
                    item.SubItems.Add(hdtrasau.SSDT.ToString());
                    item.SubItems.Add(hdtrasau.FTongtien.ToString());
                    item.SubItems.Add(hdtrasau.DNgaylap.ToString("dd/MM/yyyy"));
                    item.SubItems.Add(hdtrasau.Ithoigian.ToString());
                    item.SubItems.Add(hdtrasau.ITinnhangui.ToString());
                    lv_tbtrasau.Items.Add(item);
                }
            }
        }
        private void loadtxtdoanhthutrasau()
        {
            int iMaloaitb = 1;
            List<string> listparameter = new List<string> { "@iMaLoai", "@dNgaycantke", "@dNgaybatdautke" };

            DateTime dt_batdau = Convert.ToDateTime(dt_picktrasautu.Value.ToString());
            DateTime dt_den = Convert.ToDateTime(dt_picktrasauden.Value.ToString());
            object[] parameter = new object[] { iMaloaitb, dt_den, dt_batdau };
            string sum = HoadonthuebaotrasauDAO.Instance.gettong(parameter, listparameter);
            txt_tongtientrasau.Text = sum;
        }
        private void loadNvDetail()
        {
            string id = "";
            using (StreamReader sr = File.OpenText(fDangnhap.tmpfile))
            {
                id = sr.ReadLine();
            }
            DataTable dt = dtp.ExcuteQuery("sp_getNvById", new object[] { id }, new List<string> { "@id" });
            if (dt != null && dt.Rows.Count > 0)
            {
                txtUser.Text = dt.Rows[0]["sUserName"].ToString();
                txtNvName.Text = dt.Rows[0]["sTenNV"].ToString();
                txtNvUsername.Text = dt.Rows[0]["sUserName"].ToString();
                txtNvRole.Text = (dt.Rows[0]["bLoaiAcc"].ToString() == "0" ? "Admin" : "Nhân Viên");
                txtNvBirthday.Text = dt.Rows[0]["dNgaySinh"].ToString().Split(' ')[0] + "";
                txtNvJoinDate.Text = dt.Rows[0]["dNgayvaolam"].ToString().Split(' ')[0] + "";
            }
            else
            {
                MessageBox.Show("Lấy Thông Tin Không Thành Công");
            }
        }

        private void btnSignOut_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Đăng Xuất ?", "Đăng Xuất", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
            {
                fDangnhap.DeleteTmpFile(fDangnhap.tmpfile);
                this.Close();
            }
        }
    }
}
