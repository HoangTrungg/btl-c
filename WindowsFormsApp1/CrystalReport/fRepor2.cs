﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DAO;

namespace WindowsFormsApp1.CrystalReport
{
    public partial class fRepor2 : Form
    {
        public fRepor2()
        {
            InitializeComponent();
        }

        private void fRepor2_Load(object sender, EventArgs e)
        {
            CrystalReport1 report = new CrystalReport1();
            string query = "SELECT dbo.tblHoaDon.*,dbo.tblThueBao.sTenThueBao,dbo.tblThueBao.bGioitinh,dbo.tblThueBao.sSDT,dbo.tblThueBao.iMaLoai FROM dbo.tblHoaDon,dbo.tblThueBao WHERE dbo.tblHoaDon.iMaThueBao = dbo.tblThueBao.iMaThueBao AND dbo.tblThueBao.iMaLoai = 2";
            report.SetDataSource(new DataProvider().gettable(query));
            crystalReportViewer1.ReportSource = report;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
