﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DAO;
using WindowsFormsApp1.DTO;

namespace WindowsFormsApp1
{
    public partial class fTaotaikhoan : Form
    {
        public fTaotaikhoan()
        {
            InitializeComponent();
            loadcb_nv();
        }

        private void Btn_them_Click(object sender, EventArgs e)
        {
            if (valid() == false)
            {
                MessageBox.Show("Chưa điền đầy đủ thông tin");
                return;
            }
            else
            {
                string md5bum = bum.Hash(txt_pass.Text);
                List<string> listpara = new List<string> { "@sUsername", "@sPassword", "@iID_NV" };
                string sUser = txt_user.Text.Trim();
                int maNV = int.Parse(cb_nhanvien.SelectedValue.ToString());
                object[] parameter = new object[] { sUser, md5bum, maNV };
                if (AccountNVDAO.Instance.insert(parameter, listpara))
                {
                    MessageBox.Show("Thành công");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Xem lại thông tin vừa nhập hoặc có thể nhân viên đã có tài khoản !!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        private void loadcb_nv()
        {
            List<NhanVienDTO> listnv = NhanvienDAO.Instance.getdata(null, null);
            cb_nhanvien.DataSource = listnv;
            cb_nhanvien.DisplayMember = "STenNV";
            cb_nhanvien.ValueMember = "IMaNV";
            cb_nhanvien.SelectedIndex = -1;
        }

        private void btnCancelAddAccount_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Hủy thao tác ?", "Hủy", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
            {
                Clear();
                this.Close();
            }
        }
        private void Clear()
        {
            txt_pass.Text = "";
            txt_user.Text = "";
        }
        private Boolean valid()
        {
            if (cb_nhanvien.SelectedValue == null || txt_pass.Text == "" || txt_user.Text == "")
            {
                return false;
            }
            return true;
        }
    }
}
