﻿namespace WindowsFormsApp1
{
    partial class fTaotaikhoan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cb_nhanvien = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_them = new System.Windows.Forms.Button();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.btnCancelAddAccount = new System.Windows.Forms.Button();
            this.txt_user = new MetroFramework.Controls.MetroTextBox();
            this.txt_pass = new MetroFramework.Controls.MetroTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 78);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "UserName";
            // 
            // cb_nhanvien
            // 
            this.cb_nhanvien.FormattingEnabled = true;
            this.cb_nhanvien.Location = new System.Drawing.Point(125, 151);
            this.cb_nhanvien.Margin = new System.Windows.Forms.Padding(2);
            this.cb_nhanvien.Name = "cb_nhanvien";
            this.cb_nhanvien.Size = new System.Drawing.Size(289, 21);
            this.cb_nhanvien.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 117);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 159);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nhân Viên";
            // 
            // btn_them
            // 
            this.btn_them.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_them.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btn_them.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_them.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.btn_them.ForeColor = System.Drawing.Color.White;
            this.btn_them.Location = new System.Drawing.Point(325, 199);
            this.btn_them.Margin = new System.Windows.Forms.Padding(2);
            this.btn_them.Name = "btn_them";
            this.btn_them.Size = new System.Drawing.Size(89, 39);
            this.btn_them.TabIndex = 4;
            this.btn_them.Text = "Thêm";
            this.btn_them.UseVisualStyleBackColor = false;
            this.btn_them.Click += new System.EventHandler(this.Btn_them_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.metroLabel1.Location = new System.Drawing.Point(168, 9);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(246, 25);
            this.metroLabel1.TabIndex = 4;
            this.metroLabel1.Text = "Cấp Tài Khoản Cho Nhân Viên";
            this.metroLabel1.UseCustomBackColor = true;
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // btnCancelAddAccount
            // 
            this.btnCancelAddAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelAddAccount.BackColor = System.Drawing.Color.Red;
            this.btnCancelAddAccount.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkRed;
            this.btnCancelAddAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelAddAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.btnCancelAddAccount.ForeColor = System.Drawing.Color.White;
            this.btnCancelAddAccount.Location = new System.Drawing.Point(484, 311);
            this.btnCancelAddAccount.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancelAddAccount.Name = "btnCancelAddAccount";
            this.btnCancelAddAccount.Size = new System.Drawing.Size(89, 39);
            this.btnCancelAddAccount.TabIndex = 5;
            this.btnCancelAddAccount.Text = "Hủy";
            this.btnCancelAddAccount.UseVisualStyleBackColor = false;
            this.btnCancelAddAccount.Click += new System.EventHandler(this.btnCancelAddAccount_Click);
            // 
            // txt_user
            // 
            // 
            // 
            // 
            this.txt_user.CustomButton.Image = null;
            this.txt_user.CustomButton.Location = new System.Drawing.Point(267, 1);
            this.txt_user.CustomButton.Name = "";
            this.txt_user.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txt_user.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txt_user.CustomButton.TabIndex = 1;
            this.txt_user.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txt_user.CustomButton.UseSelectable = true;
            this.txt_user.CustomButton.Visible = false;
            this.txt_user.DisplayIcon = true;
            this.txt_user.Icon = global::WindowsFormsApp1.Properties.Resources.user;
            this.txt_user.Lines = new string[0];
            this.txt_user.Location = new System.Drawing.Point(125, 68);
            this.txt_user.MaxLength = 32767;
            this.txt_user.Name = "txt_user";
            this.txt_user.PasswordChar = '\0';
            this.txt_user.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_user.SelectedText = "";
            this.txt_user.SelectionLength = 0;
            this.txt_user.SelectionStart = 0;
            this.txt_user.ShortcutsEnabled = true;
            this.txt_user.ShowClearButton = true;
            this.txt_user.Size = new System.Drawing.Size(289, 23);
            this.txt_user.TabIndex = 1;
            this.txt_user.UseSelectable = true;
            this.txt_user.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txt_user.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txt_pass
            // 
            // 
            // 
            // 
            this.txt_pass.CustomButton.Image = null;
            this.txt_pass.CustomButton.Location = new System.Drawing.Point(267, 1);
            this.txt_pass.CustomButton.Name = "";
            this.txt_pass.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txt_pass.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txt_pass.CustomButton.TabIndex = 1;
            this.txt_pass.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txt_pass.CustomButton.UseSelectable = true;
            this.txt_pass.CustomButton.Visible = false;
            this.txt_pass.DisplayIcon = true;
            this.txt_pass.Icon = global::WindowsFormsApp1.Properties.Resources.padlock;
            this.txt_pass.Lines = new string[0];
            this.txt_pass.Location = new System.Drawing.Point(125, 107);
            this.txt_pass.MaxLength = 32767;
            this.txt_pass.Name = "txt_pass";
            this.txt_pass.PasswordChar = '●';
            this.txt_pass.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_pass.SelectedText = "";
            this.txt_pass.SelectionLength = 0;
            this.txt_pass.SelectionStart = 0;
            this.txt_pass.ShortcutsEnabled = true;
            this.txt_pass.ShowClearButton = true;
            this.txt_pass.Size = new System.Drawing.Size(289, 23);
            this.txt_pass.TabIndex = 2;
            this.txt_pass.UseSelectable = true;
            this.txt_pass.UseSystemPasswordChar = true;
            this.txt_pass.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txt_pass.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // fTaotaikhoan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.txt_pass);
            this.Controls.Add(this.txt_user);
            this.Controls.Add(this.btnCancelAddAccount);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.btn_them);
            this.Controls.Add(this.cb_nhanvien);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "fTaotaikhoan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fTaotaikhoan";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_nhanvien;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_them;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.Button btnCancelAddAccount;
        private MetroFramework.Controls.MetroTextBox txt_user;
        private MetroFramework.Controls.MetroTextBox txt_pass;
    }
}