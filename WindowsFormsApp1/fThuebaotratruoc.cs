﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DAO;
using WindowsFormsApp1.DTO;

namespace WindowsFormsApp1
{
    public partial class fThuebaotratruoc : Form
    {
        public fThuebaotratruoc()
        {
            InitializeComponent();
            loadlisttratruoc();
            btn_sua.Enabled = false;
        }
        List<TratruocDTO> list_tratruoc;
        private Boolean flag = false;
        private void loadlisttratruoc()
        {
            lv_tratruoc.Items.Clear();
            list_tratruoc = TratruocDAO.Instance.getdatanvtratruoc(null, null);
            ListViewItem item;
            foreach (TratruocDTO tratruoc in list_tratruoc)
            {
                item = new ListViewItem();
                item.Text = tratruoc.IMathuebao.ToString();
                item.SubItems.Add(tratruoc.IMaloai.ToString());
                item.SubItems.Add(tratruoc.STenthuebao.ToString());
                item.SubItems.Add(tratruoc.bGioitinh == true ? "Nu" : "Nam");
                item.SubItems.Add(tratruoc.SSDT.ToString());
                item.SubItems.Add(tratruoc.ITuoi.ToString());
                item.SubItems.Add(tratruoc.SCMND.ToString());
                item.SubItems.Add(tratruoc.DNgaydangky.ToString("dd/MM/yyyy"));
                lv_tratruoc.Items.Add(item);
            }
        }

        private void Button10_Click(object sender, EventArgs e)
        {
            List<string> listpara = new List<string> { "@sTenthuebao", "@bGioitinh", "@sSDT", "@iTuoi", "@sCMND", "@dNgaydangky" };
            string sTenthuebao = txt_tenthuebao.Text.Trim();
            bool bGT = rd_nam.Checked ? false : true;
            string sdt = txt_sdt.Text.Trim();
            int tuoi = int.Parse(txt_tuoi.Text);
            string sCMND = txt_cmnd.Text.Trim();
            DateTime dNgaydangky = Convert.ToDateTime(dt_pickngaydangky.Value.ToString());
            object[] parameter = new object[] { sTenthuebao, bGT, sdt, tuoi, sCMND, dNgaydangky };
            if (TratruocDAO.Instance.insert(parameter, listpara))
            {
                MessageBox.Show("Thanh cong !");
                loadlisttratruoc();

            }
            else
                MessageBox.Show("That bai");
        }

        private void Txt_tuoi_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void Lv_tratruoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataToggle();
            btn_sua.Enabled = true;
            btn_them.Enabled = false;
            btn_huy.Enabled = true;
            if (lv_tratruoc.SelectedItems.Count <= 0)
                return;
            txt_mathuebao.Text = lv_tratruoc.SelectedItems[0].Text;
            txt_loaithuebao.Text = lv_tratruoc.SelectedItems[0].SubItems[1].Text.ToString();
            txt_tenthuebao.Text = lv_tratruoc.SelectedItems[0].SubItems[2].Text.ToString();
            if (lv_tratruoc.SelectedItems[0].SubItems[3].Text.ToString().Equals("Nu"))
            {
                rd_nu.Checked = true;
            }
            else
                rd_nam.Checked = true;
            txt_sdt.Text = lv_tratruoc.SelectedItems[0].SubItems[4].Text.ToString();
            txt_tuoi.Text = lv_tratruoc.SelectedItems[0].SubItems[5].Text.ToString();
            txt_cmnd.Text = lv_tratruoc.SelectedItems[0].SubItems[6].Text.ToString();
            //dt_pickngaydangky.Value = DateTime.ParseExact(lv_tratruoc.SelectedItems[0].SubItems[7].Text, "dd/MM/yyyy", new CultureInfo("vi-VN"));
            dt_pickngaydangky.Value = DateTime.ParseExact(lv_tratruoc.SelectedItems[0].SubItems[7].Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

        }

        private void Btn_sua_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Sửa thông tin ?", "Sửa", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
            {
                List<String> listpara = new List<string> { "@iMaThueBao", "@iMaLoai", "@sTenThueBao", "@bGioitinh", "@sSDT", "@iTuoi", "@sCMND", "@dNgaydangky" };
                int iMaThuebao = int.Parse(txt_mathuebao.Text);
                int iMaLoai = int.Parse(txt_loaithuebao.Text);
                int ituoi = int.Parse(txt_tuoi.Text);
                string sTenThuebao = txt_tenthuebao.Text;
                string sCMND = txt_cmnd.Text;
                bool bGT = rd_nam.Checked ? false : true;
                string sSDT = txt_sdt.Text;
                DateTime dNgaydangky = Convert.ToDateTime(dt_pickngaydangky.Value.ToString());

                object[] parameter = new object[] { iMaThuebao, iMaLoai, sTenThuebao, bGT, sSDT, ituoi, sCMND, dNgaydangky };
                if (TratruocDAO.Instance.update(parameter, listpara))
                {
                    btn_them.Enabled = true;
                    btn_sua.Enabled = false;
                    btn_huy.Enabled = true;
                    Clear();
                    flag = false;
                    dataToggle();
                    MessageBox.Show("Sửa Thành Công !!");
                    loadlisttratruoc();
                }
                else
                {
                    MessageBox.Show("Lỗi!");
                }
            }
            else
            {
                return;
            }


        }

        private void Txt_sdt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void Txt_cmnd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void Btn_huy_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ngừng cung cấp dịch vụ ?", "Ngừng", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
            {
                List<string> listpara = new List<string> { "@iMathuebao", "@bTrangthai" };
                int itrangthai = 1;
                int iMatb = int.Parse(txt_mathuebao.Text);
                object[] parameter = new object[] { iMatb, itrangthai };
                if (TrasauDAO.Instance.updatetrangthai(parameter, listpara))
                {
                    MessageBox.Show("Đã ngưng sử dụng dịch vụ đối với " + txt_tenthuebao.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    flag = false;
                    Clear();
                    dataToggle();
                    loadlisttratruoc();
                }
                else
                {
                    MessageBox.Show("Chưa thể ngưng dịch vụ của" + txt_tenthuebao.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                return;
            }

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Clear();
            this.Close();
        }
        private void Clear()
        {
            txt_mathuebao.Text = "";
            txt_loaithuebao.Text = "";
            txt_cmnd.Text = "";
            txt_sdt.Text = "";
            txt_tuoi.Text = "";
            txt_tenthuebao.Text = "";
            dt_pickngaydangky.Value = DateTime.Now;
        }
        private void dataToggle()
        {
            if (flag == false)
            {
                txt_tenthuebao.Enabled = txt_tenthuebao.Enabled == true ? false : true;
                rd_nam.Enabled = rd_nam.Enabled == true ? false : true;
                rd_nu.Enabled = rd_nu.Enabled == true ? false : true;
                txt_sdt.Enabled = txt_sdt.Enabled == true ? false : true;
                txt_cmnd.Enabled = txt_cmnd.Enabled == true ? false : true;
                txt_tuoi.Enabled = txt_tuoi.Enabled == true ? false : true;
                dt_pickngaydangky.Enabled = dt_pickngaydangky.Enabled == true ? false : true;
                flag = true;
            }
            else
            {
                return;
            }

        }
    }
}
