﻿namespace WindowsFormsApp1
{
    partial class fNhanvien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tàiKhoảnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thôngTinTàiKhoảnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.đăngXuấtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLýThuêBaoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLýThuêBaoTrảSauToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLýThuêBaoTrảTrướcToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thuêBaoĐãNgừngSửDụngDịchVụToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnPrintTB = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lv_hdthuebaotratruoc = new System.Windows.Forms.ListView();
            this.iMaHD = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.iMaThueBao = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.iMaNV = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.iMaLoai = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sTenThueBao = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bGioitinh = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sSDT = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.fTongTien = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dNgaylap = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.fThoigiangoi = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.fTinnhangui = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel2 = new System.Windows.Forms.Panel();
            this.lv_thuebaotrasau = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnPrintTB2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tàiKhoảnToolStripMenuItem,
            this.quảnLýThuêBaoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1300, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tàiKhoảnToolStripMenuItem
            // 
            this.tàiKhoảnToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thôngTinTàiKhoảnToolStripMenuItem,
            this.đăngXuấtToolStripMenuItem});
            this.tàiKhoảnToolStripMenuItem.Name = "tàiKhoảnToolStripMenuItem";
            this.tàiKhoảnToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.tàiKhoảnToolStripMenuItem.Text = "Tài khoản";
            // 
            // thôngTinTàiKhoảnToolStripMenuItem
            // 
            this.thôngTinTàiKhoảnToolStripMenuItem.Name = "thôngTinTàiKhoảnToolStripMenuItem";
            this.thôngTinTàiKhoảnToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.thôngTinTàiKhoảnToolStripMenuItem.Text = "Thông tin tài khoản";
            this.thôngTinTàiKhoảnToolStripMenuItem.Click += new System.EventHandler(this.ThôngTinTàiKhoảnToolStripMenuItem_Click);
            // 
            // đăngXuấtToolStripMenuItem
            // 
            this.đăngXuấtToolStripMenuItem.Name = "đăngXuấtToolStripMenuItem";
            this.đăngXuấtToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.đăngXuấtToolStripMenuItem.Text = "Đăng xuất";
            this.đăngXuấtToolStripMenuItem.Click += new System.EventHandler(this.đăngXuấtToolStripMenuItem_Click);
            // 
            // quảnLýThuêBaoToolStripMenuItem
            // 
            this.quảnLýThuêBaoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quảnLýThuêBaoTrảSauToolStripMenuItem,
            this.quảnLýThuêBaoTrảTrướcToolStripMenuItem,
            this.thuêBaoĐãNgừngSửDụngDịchVụToolStripMenuItem});
            this.quảnLýThuêBaoToolStripMenuItem.Name = "quảnLýThuêBaoToolStripMenuItem";
            this.quảnLýThuêBaoToolStripMenuItem.Size = new System.Drawing.Size(110, 20);
            this.quảnLýThuêBaoToolStripMenuItem.Text = "Quản lý thuê bao";
            // 
            // quảnLýThuêBaoTrảSauToolStripMenuItem
            // 
            this.quảnLýThuêBaoTrảSauToolStripMenuItem.Name = "quảnLýThuêBaoTrảSauToolStripMenuItem";
            this.quảnLýThuêBaoTrảSauToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.quảnLýThuêBaoTrảSauToolStripMenuItem.Text = "Quản lý thuê bao trả sau";
            this.quảnLýThuêBaoTrảSauToolStripMenuItem.Click += new System.EventHandler(this.QuảnLýThuêBaoTrảSauToolStripMenuItem_Click);
            // 
            // quảnLýThuêBaoTrảTrướcToolStripMenuItem
            // 
            this.quảnLýThuêBaoTrảTrướcToolStripMenuItem.Name = "quảnLýThuêBaoTrảTrướcToolStripMenuItem";
            this.quảnLýThuêBaoTrảTrướcToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.quảnLýThuêBaoTrảTrướcToolStripMenuItem.Text = "Quản lý thuê bao trả trước";
            this.quảnLýThuêBaoTrảTrướcToolStripMenuItem.Click += new System.EventHandler(this.QuảnLýThuêBaoTrảTrướcToolStripMenuItem_Click);
            // 
            // thuêBaoĐãNgừngSửDụngDịchVụToolStripMenuItem
            // 
            this.thuêBaoĐãNgừngSửDụngDịchVụToolStripMenuItem.Name = "thuêBaoĐãNgừngSửDụngDịchVụToolStripMenuItem";
            this.thuêBaoĐãNgừngSửDụngDịchVụToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.thuêBaoĐãNgừngSửDụngDịchVụToolStripMenuItem.Text = "Thuê bao đã ngừng sử dụng dịch vụ";
            this.thuêBaoĐãNgừngSửDụngDịchVụToolStripMenuItem.Click += new System.EventHandler(this.ThuêBaoĐãNgừngSửDụngDịchVụToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.btnPrintTB);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lv_hdthuebaotratruoc);
            this.panel1.Location = new System.Drawing.Point(9, 25);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1280, 201);
            this.panel1.TabIndex = 1;
            // 
            // btnPrintTB
            // 
            this.btnPrintTB.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnPrintTB.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btnPrintTB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrintTB.ForeColor = System.Drawing.Color.White;
            this.btnPrintTB.Location = new System.Drawing.Point(1189, 28);
            this.btnPrintTB.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrintTB.Name = "btnPrintTB";
            this.btnPrintTB.Size = new System.Drawing.Size(89, 47);
            this.btnPrintTB.TabIndex = 1;
            this.btnPrintTB.Text = "In hóa đơn";
            this.btnPrintTB.UseVisualStyleBackColor = false;
            this.btnPrintTB.Click += new System.EventHandler(this.btnPrintTB_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(107, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(270, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Hóa đơn thuê bao trả trước";
            // 
            // lv_hdthuebaotratruoc
            // 
            this.lv_hdthuebaotratruoc.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.iMaHD,
            this.iMaThueBao,
            this.iMaNV,
            this.iMaLoai,
            this.sTenThueBao,
            this.bGioitinh,
            this.sSDT,
            this.fTongTien,
            this.dNgaylap,
            this.fThoigiangoi,
            this.fTinnhangui});
            this.lv_hdthuebaotratruoc.Location = new System.Drawing.Point(10, 27);
            this.lv_hdthuebaotratruoc.Margin = new System.Windows.Forms.Padding(2);
            this.lv_hdthuebaotratruoc.Name = "lv_hdthuebaotratruoc";
            this.lv_hdthuebaotratruoc.Size = new System.Drawing.Size(1175, 171);
            this.lv_hdthuebaotratruoc.TabIndex = 0;
            this.lv_hdthuebaotratruoc.UseCompatibleStateImageBehavior = false;
            this.lv_hdthuebaotratruoc.View = System.Windows.Forms.View.Details;
            // 
            // iMaHD
            // 
            this.iMaHD.Text = "Mã hóa đơn";
            this.iMaHD.Width = 75;
            // 
            // iMaThueBao
            // 
            this.iMaThueBao.Text = "Thuê bao số";
            this.iMaThueBao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iMaThueBao.Width = 95;
            // 
            // iMaNV
            // 
            this.iMaNV.Text = "Mã nhân viên";
            this.iMaNV.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iMaNV.Width = 89;
            // 
            // iMaLoai
            // 
            this.iMaLoai.Text = "Loại thuê bao";
            this.iMaLoai.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iMaLoai.Width = 83;
            // 
            // sTenThueBao
            // 
            this.sTenThueBao.Text = "Tên thuê bao";
            this.sTenThueBao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sTenThueBao.Width = 100;
            // 
            // bGioitinh
            // 
            this.bGioitinh.Text = "Giới tính";
            this.bGioitinh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.bGioitinh.Width = 76;
            // 
            // sSDT
            // 
            this.sSDT.Text = "SDT";
            this.sSDT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sSDT.Width = 98;
            // 
            // fTongTien
            // 
            this.fTongTien.Text = "Tổng tiền sử dụng dịch vụ";
            this.fTongTien.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.fTongTien.Width = 150;
            // 
            // dNgaylap
            // 
            this.dNgaylap.Text = "Ngày lập";
            this.dNgaylap.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dNgaylap.Width = 119;
            // 
            // fThoigiangoi
            // 
            this.fThoigiangoi.Text = "Thời lượng cuộc gọi ( phút )";
            this.fThoigiangoi.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.fThoigiangoi.Width = 150;
            // 
            // fTinnhangui
            // 
            this.fTinnhangui.Text = "Tin nhắn gửi";
            this.fTinnhangui.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.fTinnhangui.Width = 112;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.lv_thuebaotrasau);
            this.panel2.Controls.Add(this.btnPrintTB2);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(9, 231);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1280, 201);
            this.panel2.TabIndex = 1;
            // 
            // lv_thuebaotrasau
            // 
            this.lv_thuebaotrasau.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11});
            this.lv_thuebaotrasau.Location = new System.Drawing.Point(10, 31);
            this.lv_thuebaotrasau.Margin = new System.Windows.Forms.Padding(2);
            this.lv_thuebaotrasau.Name = "lv_thuebaotrasau";
            this.lv_thuebaotrasau.Size = new System.Drawing.Size(1175, 171);
            this.lv_thuebaotrasau.TabIndex = 3;
            this.lv_thuebaotrasau.UseCompatibleStateImageBehavior = false;
            this.lv_thuebaotrasau.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Mã hóa đơn";
            this.columnHeader1.Width = 77;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Thuê bao số";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 98;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Mã nhân viên";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 89;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Loại thuê bao";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 89;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Tên thuê bao";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 106;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Giới tính";
            this.columnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader6.Width = 80;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "SDT";
            this.columnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader7.Width = 102;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Tổng tiền sử dụng dịch vụ";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader8.Width = 150;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Ngày lập";
            this.columnHeader9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader9.Width = 78;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Thời lượng cuộc gọi ( phút )";
            this.columnHeader10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader10.Width = 167;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Tin nhắn gửi";
            this.columnHeader11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader11.Width = 104;
            // 
            // btnPrintTB2
            // 
            this.btnPrintTB2.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnPrintTB2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btnPrintTB2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrintTB2.ForeColor = System.Drawing.Color.White;
            this.btnPrintTB2.Location = new System.Drawing.Point(1189, 31);
            this.btnPrintTB2.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrintTB2.Name = "btnPrintTB2";
            this.btnPrintTB2.Size = new System.Drawing.Size(89, 47);
            this.btnPrintTB2.TabIndex = 2;
            this.btnPrintTB2.Text = "In hóa đơn";
            this.btnPrintTB2.UseVisualStyleBackColor = false;
            this.btnPrintTB2.Click += new System.EventHandler(this.btnPrintTB2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label2.Location = new System.Drawing.Point(107, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(251, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Hóa đơn thuê bao trả sau";
            // 
            // fNhanvien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 500);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "fNhanvien";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fQuanly";
            this.VisibleChanged += new System.EventHandler(this.fNhanvien_VisibleChanged);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tàiKhoảnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thôngTinTàiKhoảnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem đăngXuấtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLýThuêBaoToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView lv_hdthuebaotratruoc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem quảnLýThuêBaoTrảSauToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLýThuêBaoTrảTrướcToolStripMenuItem;
        private System.Windows.Forms.Button btnPrintTB;
        private System.Windows.Forms.Button btnPrintTB2;
        private System.Windows.Forms.ColumnHeader iMaHD;
        private System.Windows.Forms.ColumnHeader iMaThueBao;
        private System.Windows.Forms.ColumnHeader iMaNV;
        private System.Windows.Forms.ColumnHeader iMaLoai;
        private System.Windows.Forms.ColumnHeader sTenThueBao;
        private System.Windows.Forms.ColumnHeader bGioitinh;
        private System.Windows.Forms.ColumnHeader sSDT;
        private System.Windows.Forms.ColumnHeader fTongTien;
        private System.Windows.Forms.ColumnHeader dNgaylap;
        private System.Windows.Forms.ColumnHeader fThoigiangoi;
        private System.Windows.Forms.ColumnHeader fTinnhangui;
        private System.Windows.Forms.ListView lv_thuebaotrasau;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ToolStripMenuItem thuêBaoĐãNgừngSửDụngDịchVụToolStripMenuItem;
    }
}