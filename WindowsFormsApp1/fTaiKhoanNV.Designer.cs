﻿namespace WindowsFormsApp1
{
    partial class fTaiKhoanNV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblOldPass = new System.Windows.Forms.Label();
            this.lblNewPass = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtOldPass = new System.Windows.Forms.TextBox();
            this.txtNewPass = new System.Windows.Forms.TextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnCancelAddAccount = new System.Windows.Forms.Button();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtNvCode = new MetroFramework.Controls.MetroLabel();
            this.txtNvName = new MetroFramework.Controls.MetroLabel();
            this.txtNvUsername = new MetroFramework.Controls.MetroLabel();
            this.txtRePass = new System.Windows.Forms.TextBox();
            this.lblRePass = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNvRole = new MetroFramework.Controls.MetroLabel();
            this.tggPass = new MetroFramework.Controls.MetroToggle();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 57);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mã nhân viên";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 105);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Tên nhân viên";
            // 
            // lblOldPass
            // 
            this.lblOldPass.AutoSize = true;
            this.lblOldPass.Location = new System.Drawing.Point(285, 57);
            this.lblOldPass.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblOldPass.Name = "lblOldPass";
            this.lblOldPass.Size = new System.Drawing.Size(67, 13);
            this.lblOldPass.TabIndex = 0;
            this.lblOldPass.Text = "Mật khẩu cũ";
            this.lblOldPass.Visible = false;
            // 
            // lblNewPass
            // 
            this.lblNewPass.AutoSize = true;
            this.lblNewPass.Location = new System.Drawing.Point(285, 105);
            this.lblNewPass.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNewPass.Name = "lblNewPass";
            this.lblNewPass.Size = new System.Drawing.Size(71, 13);
            this.lblNewPass.TabIndex = 0;
            this.lblNewPass.Text = "Mật khẩu mới";
            this.lblNewPass.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 157);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Tên đăng nhập";
            // 
            // txtOldPass
            // 
            this.txtOldPass.Location = new System.Drawing.Point(412, 54);
            this.txtOldPass.Margin = new System.Windows.Forms.Padding(2);
            this.txtOldPass.Name = "txtOldPass";
            this.txtOldPass.PasswordChar = '●';
            this.txtOldPass.Size = new System.Drawing.Size(129, 20);
            this.txtOldPass.TabIndex = 1;
            this.txtOldPass.Visible = false;
            // 
            // txtNewPass
            // 
            this.txtNewPass.Location = new System.Drawing.Point(412, 102);
            this.txtNewPass.Margin = new System.Windows.Forms.Padding(2);
            this.txtNewPass.Name = "txtNewPass";
            this.txtNewPass.PasswordChar = '●';
            this.txtNewPass.Size = new System.Drawing.Size(129, 20);
            this.txtNewPass.TabIndex = 2;
            this.txtNewPass.Visible = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnUpdate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(450, 195);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(91, 35);
            this.btnUpdate.TabIndex = 4;
            this.btnUpdate.Text = "Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Visible = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnCancelAddAccount
            // 
            this.btnCancelAddAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelAddAccount.BackColor = System.Drawing.Color.Red;
            this.btnCancelAddAccount.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkRed;
            this.btnCancelAddAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelAddAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.btnCancelAddAccount.ForeColor = System.Drawing.Color.White;
            this.btnCancelAddAccount.Location = new System.Drawing.Point(468, 272);
            this.btnCancelAddAccount.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancelAddAccount.Name = "btnCancelAddAccount";
            this.btnCancelAddAccount.Size = new System.Drawing.Size(89, 39);
            this.btnCancelAddAccount.TabIndex = 5;
            this.btnCancelAddAccount.Text = "Trở Về";
            this.btnCancelAddAccount.UseVisualStyleBackColor = false;
            this.btnCancelAddAccount.Click += new System.EventHandler(this.btnCancelAddAccount_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.metroLabel1.Location = new System.Drawing.Point(163, 9);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(177, 25);
            this.metroLabel1.TabIndex = 7;
            this.metroLabel1.Text = "Thông Tin Nhân Viên";
            this.metroLabel1.UseCustomBackColor = true;
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // txtNvCode
            // 
            this.txtNvCode.AutoSize = true;
            this.txtNvCode.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.txtNvCode.Location = new System.Drawing.Point(126, 55);
            this.txtNvCode.Name = "txtNvCode";
            this.txtNvCode.Size = new System.Drawing.Size(58, 19);
            this.txtNvCode.TabIndex = 8;
            this.txtNvCode.Text = "NvCode";
            this.txtNvCode.UseCustomBackColor = true;
            // 
            // txtNvName
            // 
            this.txtNvName.AutoSize = true;
            this.txtNvName.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.txtNvName.Location = new System.Drawing.Point(126, 103);
            this.txtNvName.Name = "txtNvName";
            this.txtNvName.Size = new System.Drawing.Size(62, 19);
            this.txtNvName.TabIndex = 9;
            this.txtNvName.Text = "NvName";
            this.txtNvName.UseCustomBackColor = true;
            // 
            // txtNvUsername
            // 
            this.txtNvUsername.AutoSize = true;
            this.txtNvUsername.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.txtNvUsername.Location = new System.Drawing.Point(126, 155);
            this.txtNvUsername.Name = "txtNvUsername";
            this.txtNvUsername.Size = new System.Drawing.Size(88, 19);
            this.txtNvUsername.TabIndex = 10;
            this.txtNvUsername.Text = "NvUsername";
            this.txtNvUsername.UseCustomBackColor = true;
            // 
            // txtRePass
            // 
            this.txtRePass.Location = new System.Drawing.Point(412, 154);
            this.txtRePass.Margin = new System.Windows.Forms.Padding(2);
            this.txtRePass.Name = "txtRePass";
            this.txtRePass.PasswordChar = '●';
            this.txtRePass.Size = new System.Drawing.Size(129, 20);
            this.txtRePass.TabIndex = 3;
            this.txtRePass.Visible = false;
            this.txtRePass.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRePass_KeyUp);
            // 
            // lblRePass
            // 
            this.lblRePass.AutoSize = true;
            this.lblRePass.Location = new System.Drawing.Point(285, 157);
            this.lblRePass.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRePass.Name = "lblRePass";
            this.lblRePass.Size = new System.Drawing.Size(100, 13);
            this.lblRePass.TabIndex = 11;
            this.lblRePass.Text = "Xác nhận mật khẩu";
            this.lblRePass.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 195);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Chức vụ";
            // 
            // txtNvRole
            // 
            this.txtNvRole.AutoSize = true;
            this.txtNvRole.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.txtNvRole.Location = new System.Drawing.Point(126, 193);
            this.txtNvRole.Name = "txtNvRole";
            this.txtNvRole.Size = new System.Drawing.Size(52, 19);
            this.txtNvRole.TabIndex = 14;
            this.txtNvRole.Text = "NvRole";
            this.txtNvRole.UseCustomBackColor = true;
            // 
            // tggPass
            // 
            this.tggPass.AutoSize = true;
            this.tggPass.BackColor = System.Drawing.Color.Transparent;
            this.tggPass.Location = new System.Drawing.Point(126, 233);
            this.tggPass.Name = "tggPass";
            this.tggPass.Size = new System.Drawing.Size(80, 17);
            this.tggPass.TabIndex = 6;
            this.tggPass.Text = "Off";
            this.tggPass.UseCustomBackColor = true;
            this.tggPass.UseCustomForeColor = true;
            this.tggPass.UseSelectable = true;
            this.tggPass.CheckedChanged += new System.EventHandler(this.tggPass_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 237);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Đổi mật khẩu";
            // 
            // fTaiKhoanNV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 322);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tggPass);
            this.Controls.Add(this.txtNvRole);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtRePass);
            this.Controls.Add(this.lblRePass);
            this.Controls.Add(this.txtNvUsername);
            this.Controls.Add(this.txtNvName);
            this.Controls.Add(this.txtNvCode);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.btnCancelAddAccount);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.txtNewPass);
            this.Controls.Add(this.txtOldPass);
            this.Controls.Add(this.lblOldPass);
            this.Controls.Add(this.lblNewPass);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "fTaiKhoanNV";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fThongtinNV";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblOldPass;
        private System.Windows.Forms.Label lblNewPass;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtOldPass;
        private System.Windows.Forms.TextBox txtNewPass;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnCancelAddAccount;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel txtNvCode;
        private MetroFramework.Controls.MetroLabel txtNvName;
        private MetroFramework.Controls.MetroLabel txtNvUsername;
        private System.Windows.Forms.TextBox txtRePass;
        private System.Windows.Forms.Label lblRePass;
        private System.Windows.Forms.Label label7;
        private MetroFramework.Controls.MetroLabel txtNvRole;
        private MetroFramework.Controls.MetroToggle tggPass;
        private System.Windows.Forms.Label label1;
    }
}