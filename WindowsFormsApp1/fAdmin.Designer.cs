﻿namespace WindowsFormsApp1
{
    partial class fAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.label13 = new System.Windows.Forms.Label();
            this.lv_tratruoc = new System.Windows.Forms.ListView();
            this.iMaHD = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.iMaThueBao = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.iMaLoai = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sTenThueBao = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sSDT = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.fTongTien = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dNgaylap = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.fThoigiangoi = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.fTinnhangui = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btn_xem = new System.Windows.Forms.Button();
            this.txt_tongtientratruoc = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dt_pickden = new System.Windows.Forms.DateTimePicker();
            this.dt_picktu = new System.Windows.Forms.DateTimePicker();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lbNoData = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.lv_tbtrasau = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btn_thongke = new System.Windows.Forms.Button();
            this.VND = new System.Windows.Forms.Label();
            this.txt_tongtientrasau = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dt_picktrasauden = new System.Windows.Forms.DateTimePicker();
            this.dt_picktrasautu = new System.Windows.Forms.DateTimePicker();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.lv_nhanvien = new System.Windows.Forms.ListView();
            this.iMaNV = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sTenNV = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bGioitinh = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dNgaysinh = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dNgayvaolam = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rd_nu = new System.Windows.Forms.RadioButton();
            this.rd_nam = new System.Windows.Forms.RadioButton();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dt_pickngayvaolam = new System.Windows.Forms.DateTimePicker();
            this.dt_pickngaysinh = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.txt_tennv = new System.Windows.Forms.TextBox();
            this.txt_manv = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtUser = new MetroFramework.Controls.MetroLabel();
            this.txtNvJoinDate = new MetroFramework.Controls.MetroLabel();
            this.txtNvBirthday = new MetroFramework.Controls.MetroLabel();
            this.txtNvRole = new MetroFramework.Controls.MetroLabel();
            this.txtNvUsername = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtNvName = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.label16 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.passUpdate = new System.Windows.Forms.Button();
            this.repass = new System.Windows.Forms.TextBox();
            this.newpass = new System.Windows.Forms.TextBox();
            this.oldpass = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.btnSignOut = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(10, 10);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1264, 441);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.metroLabel8);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.lv_tratruoc);
            this.tabPage1.Controls.Add(this.btn_xem);
            this.tabPage1.Controls.Add(this.txt_tongtientratruoc);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.dt_pickden);
            this.tabPage1.Controls.Add(this.dt_picktu);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(1256, 415);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Thuê bao trả trước";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel8.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel8.ForeColor = System.Drawing.Color.DodgerBlue;
            this.metroLabel8.Location = new System.Drawing.Point(1122, 17);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(41, 25);
            this.metroLabel8.TabIndex = 8;
            this.metroLabel8.Text = "Lọc";
            this.metroLabel8.UseCustomForeColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1212, 155);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "VND";
            // 
            // lv_tratruoc
            // 
            this.lv_tratruoc.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.iMaHD,
            this.iMaThueBao,
            this.columnHeader10,
            this.iMaLoai,
            this.sTenThueBao,
            this.columnHeader11,
            this.sSDT,
            this.fTongTien,
            this.dNgaylap,
            this.fThoigiangoi,
            this.fTinnhangui});
            this.lv_tratruoc.Location = new System.Drawing.Point(0, 3);
            this.lv_tratruoc.Margin = new System.Windows.Forms.Padding(2);
            this.lv_tratruoc.Name = "lv_tratruoc";
            this.lv_tratruoc.Size = new System.Drawing.Size(1021, 364);
            this.lv_tratruoc.TabIndex = 6;
            this.lv_tratruoc.UseCompatibleStateImageBehavior = false;
            this.lv_tratruoc.View = System.Windows.Forms.View.Details;
            // 
            // iMaHD
            // 
            this.iMaHD.Text = "Mã hóa đơn";
            this.iMaHD.Width = 80;
            // 
            // iMaThueBao
            // 
            this.iMaThueBao.Text = "Thuê bao số";
            this.iMaThueBao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iMaThueBao.Width = 81;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Mã nhân viên";
            this.columnHeader10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader10.Width = 79;
            // 
            // iMaLoai
            // 
            this.iMaLoai.Text = "Loại thuê bao";
            this.iMaLoai.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iMaLoai.Width = 80;
            // 
            // sTenThueBao
            // 
            this.sTenThueBao.Text = "Tên thuê bao";
            this.sTenThueBao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sTenThueBao.Width = 107;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Giới tính";
            this.columnHeader11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader11.Width = 53;
            // 
            // sSDT
            // 
            this.sSDT.Text = "SDT";
            this.sSDT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sSDT.Width = 69;
            // 
            // fTongTien
            // 
            this.fTongTien.Text = "Tổng tiền sử dụng dịch vụ";
            this.fTongTien.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.fTongTien.Width = 139;
            // 
            // dNgaylap
            // 
            this.dNgaylap.Text = "Ngày lập";
            this.dNgaylap.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dNgaylap.Width = 87;
            // 
            // fThoigiangoi
            // 
            this.fThoigiangoi.Text = "Thời lượng cuộc gọi ( phút )";
            this.fThoigiangoi.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.fThoigiangoi.Width = 149;
            // 
            // fTinnhangui
            // 
            this.fTinnhangui.Text = "Tin nhắn gửi";
            this.fTinnhangui.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.fTinnhangui.Width = 93;
            // 
            // btn_xem
            // 
            this.btn_xem.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_xem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btn_xem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_xem.ForeColor = System.Drawing.Color.White;
            this.btn_xem.Location = new System.Drawing.Point(1170, 190);
            this.btn_xem.Margin = new System.Windows.Forms.Padding(2);
            this.btn_xem.Name = "btn_xem";
            this.btn_xem.Size = new System.Drawing.Size(72, 32);
            this.btn_xem.TabIndex = 5;
            this.btn_xem.Text = "Xem";
            this.btn_xem.UseVisualStyleBackColor = false;
            this.btn_xem.Click += new System.EventHandler(this.Btn_xem_Click);
            // 
            // txt_tongtientratruoc
            // 
            this.txt_tongtientratruoc.Location = new System.Drawing.Point(1091, 152);
            this.txt_tongtientratruoc.Margin = new System.Windows.Forms.Padding(2);
            this.txt_tongtientratruoc.Name = "txt_tongtientratruoc";
            this.txt_tongtientratruoc.Size = new System.Drawing.Size(117, 20);
            this.txt_tongtientratruoc.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1029, 154);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Doanh thu";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1029, 115);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Đến";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1029, 74);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Từ";
            // 
            // dt_pickden
            // 
            this.dt_pickden.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_pickden.Location = new System.Drawing.Point(1091, 115);
            this.dt_pickden.Margin = new System.Windows.Forms.Padding(2);
            this.dt_pickden.Name = "dt_pickden";
            this.dt_pickden.Size = new System.Drawing.Size(151, 20);
            this.dt_pickden.TabIndex = 1;
            // 
            // dt_picktu
            // 
            this.dt_picktu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_picktu.Location = new System.Drawing.Point(1091, 74);
            this.dt_picktu.Margin = new System.Windows.Forms.Padding(2);
            this.dt_picktu.Name = "dt_picktu";
            this.dt_picktu.Size = new System.Drawing.Size(151, 20);
            this.dt_picktu.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lbNoData);
            this.tabPage2.Controls.Add(this.metroLabel10);
            this.tabPage2.Controls.Add(this.lv_tbtrasau);
            this.tabPage2.Controls.Add(this.btn_thongke);
            this.tabPage2.Controls.Add(this.VND);
            this.tabPage2.Controls.Add(this.txt_tongtientrasau);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.dt_picktrasauden);
            this.tabPage2.Controls.Add(this.dt_picktrasautu);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage2.Size = new System.Drawing.Size(1256, 415);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Thuê bao trả sau";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lbNoData
            // 
            this.lbNoData.AutoSize = true;
            this.lbNoData.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbNoData.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lbNoData.Location = new System.Drawing.Point(410, 154);
            this.lbNoData.Name = "lbNoData";
            this.lbNoData.Size = new System.Drawing.Size(156, 25);
            this.lbNoData.Style = MetroFramework.MetroColorStyle.Blue;
            this.lbNoData.TabIndex = 15;
            this.lbNoData.Text = "Không Có Dữ Liệu";
            this.lbNoData.Visible = false;
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel10.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel10.ForeColor = System.Drawing.Color.DodgerBlue;
            this.metroLabel10.Location = new System.Drawing.Point(1122, 17);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(41, 25);
            this.metroLabel10.TabIndex = 14;
            this.metroLabel10.Text = "Lọc";
            this.metroLabel10.UseCustomForeColor = true;
            // 
            // lv_tbtrasau
            // 
            this.lv_tbtrasau.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader12,
            this.columnHeader13});
            this.lv_tbtrasau.Location = new System.Drawing.Point(0, 3);
            this.lv_tbtrasau.Margin = new System.Windows.Forms.Padding(2);
            this.lv_tbtrasau.Name = "lv_tbtrasau";
            this.lv_tbtrasau.Size = new System.Drawing.Size(1021, 364);
            this.lv_tbtrasau.TabIndex = 13;
            this.lv_tbtrasau.UseCompatibleStateImageBehavior = false;
            this.lv_tbtrasau.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Mã hóa đơn";
            this.columnHeader1.Width = 81;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Thuê bao số";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 77;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Mã nhân viên";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 83;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Loại thuê bao";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 88;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Tên thuê bao";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 77;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Giới tính";
            this.columnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "SDT";
            this.columnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader7.Width = 87;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Tổng tiền sử dụng dịch vụ";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader8.Width = 139;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Ngày lập";
            this.columnHeader9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader9.Width = 79;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Thời lượng cuộc gọi ( phút )";
            this.columnHeader12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader12.Width = 151;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Tin nhắn gửi";
            this.columnHeader13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader13.Width = 88;
            // 
            // btn_thongke
            // 
            this.btn_thongke.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_thongke.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btn_thongke.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_thongke.ForeColor = System.Drawing.Color.White;
            this.btn_thongke.Location = new System.Drawing.Point(1170, 190);
            this.btn_thongke.Margin = new System.Windows.Forms.Padding(2);
            this.btn_thongke.Name = "btn_thongke";
            this.btn_thongke.Size = new System.Drawing.Size(73, 33);
            this.btn_thongke.TabIndex = 12;
            this.btn_thongke.Text = "Xem";
            this.btn_thongke.UseVisualStyleBackColor = false;
            this.btn_thongke.Click += new System.EventHandler(this.Btn_thongke_Click);
            // 
            // VND
            // 
            this.VND.AutoSize = true;
            this.VND.Location = new System.Drawing.Point(1212, 155);
            this.VND.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.VND.Name = "VND";
            this.VND.Size = new System.Drawing.Size(30, 13);
            this.VND.TabIndex = 11;
            this.VND.Text = "VND";
            // 
            // txt_tongtientrasau
            // 
            this.txt_tongtientrasau.Location = new System.Drawing.Point(1091, 152);
            this.txt_tongtientrasau.Margin = new System.Windows.Forms.Padding(2);
            this.txt_tongtientrasau.Name = "txt_tongtientrasau";
            this.txt_tongtientrasau.Size = new System.Drawing.Size(117, 20);
            this.txt_tongtientrasau.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1029, 154);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Doanh thu";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1029, 115);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Đến";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1029, 74);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Từ";
            // 
            // dt_picktrasauden
            // 
            this.dt_picktrasauden.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_picktrasauden.Location = new System.Drawing.Point(1091, 115);
            this.dt_picktrasauden.Margin = new System.Windows.Forms.Padding(2);
            this.dt_picktrasauden.Name = "dt_picktrasauden";
            this.dt_picktrasauden.Size = new System.Drawing.Size(151, 20);
            this.dt_picktrasauden.TabIndex = 4;
            // 
            // dt_picktrasautu
            // 
            this.dt_picktrasautu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_picktrasautu.Location = new System.Drawing.Point(1091, 74);
            this.dt_picktrasautu.Margin = new System.Windows.Forms.Padding(2);
            this.dt_picktrasautu.Name = "dt_picktrasautu";
            this.dt_picktrasautu.Size = new System.Drawing.Size(151, 20);
            this.dt_picktrasautu.TabIndex = 5;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.metroLabel11);
            this.tabPage3.Controls.Add(this.lv_nhanvien);
            this.tabPage3.Controls.Add(this.rd_nu);
            this.tabPage3.Controls.Add(this.rd_nam);
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.dt_pickngayvaolam);
            this.tabPage3.Controls.Add(this.dt_pickngaysinh);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.textBox5);
            this.tabPage3.Controls.Add(this.txt_tennv);
            this.tabPage3.Controls.Add(this.txt_manv);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage3.Size = new System.Drawing.Size(1256, 415);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Quản lý nhân viên";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel11.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel11.ForeColor = System.Drawing.Color.DodgerBlue;
            this.metroLabel11.Location = new System.Drawing.Point(860, 27);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(193, 25);
            this.metroLabel11.TabIndex = 9;
            this.metroLabel11.Text = "Thông Tin Nhân Viên";
            this.metroLabel11.UseCustomForeColor = true;
            // 
            // lv_nhanvien
            // 
            this.lv_nhanvien.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.iMaNV,
            this.sTenNV,
            this.bGioitinh,
            this.dNgaysinh,
            this.dNgayvaolam});
            this.lv_nhanvien.FullRowSelect = true;
            this.lv_nhanvien.HideSelection = false;
            this.lv_nhanvien.Location = new System.Drawing.Point(0, 3);
            this.lv_nhanvien.Margin = new System.Windows.Forms.Padding(2);
            this.lv_nhanvien.Name = "lv_nhanvien";
            this.lv_nhanvien.Size = new System.Drawing.Size(667, 364);
            this.lv_nhanvien.TabIndex = 8;
            this.lv_nhanvien.UseCompatibleStateImageBehavior = false;
            this.lv_nhanvien.View = System.Windows.Forms.View.Details;
            this.lv_nhanvien.SelectedIndexChanged += new System.EventHandler(this.Lv_nhanvien_SelectedIndexChanged);
            // 
            // iMaNV
            // 
            this.iMaNV.Text = "Mã nhân viên";
            this.iMaNV.Width = 81;
            // 
            // sTenNV
            // 
            this.sTenNV.Text = "Tên nhân viên";
            this.sTenNV.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sTenNV.Width = 130;
            // 
            // bGioitinh
            // 
            this.bGioitinh.Text = "Giới tính";
            this.bGioitinh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.bGioitinh.Width = 71;
            // 
            // dNgaysinh
            // 
            this.dNgaysinh.Text = "Ngày sinh";
            this.dNgaysinh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dNgaysinh.Width = 131;
            // 
            // dNgayvaolam
            // 
            this.dNgayvaolam.Text = "Ngày vào làm";
            this.dNgayvaolam.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dNgayvaolam.Width = 248;
            // 
            // rd_nu
            // 
            this.rd_nu.AutoSize = true;
            this.rd_nu.Location = new System.Drawing.Point(871, 198);
            this.rd_nu.Margin = new System.Windows.Forms.Padding(2);
            this.rd_nu.Name = "rd_nu";
            this.rd_nu.Size = new System.Drawing.Size(39, 17);
            this.rd_nu.TabIndex = 7;
            this.rd_nu.Text = "Nữ";
            this.rd_nu.UseVisualStyleBackColor = true;
            // 
            // rd_nam
            // 
            this.rd_nam.AutoSize = true;
            this.rd_nam.Checked = true;
            this.rd_nam.Location = new System.Drawing.Point(785, 198);
            this.rd_nam.Margin = new System.Windows.Forms.Padding(2);
            this.rd_nam.Name = "rd_nam";
            this.rd_nam.Size = new System.Drawing.Size(47, 17);
            this.rd_nam.TabIndex = 7;
            this.rd_nam.TabStop = true;
            this.rd_nam.Text = "Nam";
            this.rd_nam.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Red;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkRed;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(1105, 244);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(68, 34);
            this.button2.TabIndex = 6;
            this.button2.Text = "Nghỉ việc";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LimeGreen;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(1015, 244);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(68, 34);
            this.button3.TabIndex = 6;
            this.button3.Text = "Cập nhật";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.DodgerBlue;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(1173, 366);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(79, 45);
            this.button1.TabIndex = 6;
            this.button1.Text = "Thêm";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // dt_pickngayvaolam
            // 
            this.dt_pickngayvaolam.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_pickngayvaolam.Location = new System.Drawing.Point(1038, 155);
            this.dt_pickngayvaolam.Margin = new System.Windows.Forms.Padding(2);
            this.dt_pickngayvaolam.Name = "dt_pickngayvaolam";
            this.dt_pickngayvaolam.Size = new System.Drawing.Size(125, 20);
            this.dt_pickngayvaolam.TabIndex = 4;
            // 
            // dt_pickngaysinh
            // 
            this.dt_pickngaysinh.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_pickngaysinh.Location = new System.Drawing.Point(1038, 106);
            this.dt_pickngaysinh.Margin = new System.Windows.Forms.Padding(2);
            this.dt_pickngaysinh.Name = "dt_pickngaysinh";
            this.dt_pickngaysinh.Size = new System.Drawing.Size(125, 20);
            this.dt_pickngaysinh.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(868, 178);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 13);
            this.label12.TabIndex = 3;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(1038, 195);
            this.textBox5.Margin = new System.Windows.Forms.Padding(2);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(125, 20);
            this.textBox5.TabIndex = 2;
            // 
            // txt_tennv
            // 
            this.txt_tennv.Location = new System.Drawing.Point(785, 155);
            this.txt_tennv.Margin = new System.Windows.Forms.Padding(2);
            this.txt_tennv.Name = "txt_tennv";
            this.txt_tennv.Size = new System.Drawing.Size(125, 20);
            this.txt_tennv.TabIndex = 2;
            // 
            // txt_manv
            // 
            this.txt_manv.Enabled = false;
            this.txt_manv.Location = new System.Drawing.Point(785, 106);
            this.txt_manv.Margin = new System.Windows.Forms.Padding(2);
            this.txt_manv.Name = "txt_manv";
            this.txt_manv.Size = new System.Drawing.Size(125, 20);
            this.txt_manv.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(929, 158);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Ngày vào làm";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(929, 109);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Ngày sinh";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(929, 198);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Tìm kiếm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(676, 198);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Giới tính";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(676, 158);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Tên nhân viên";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(676, 109);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Mã nhân viên";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.btnSignOut);
            this.tabPage4.Controls.Add(this.metroLabel6);
            this.tabPage4.Controls.Add(this.metroLabel4);
            this.tabPage4.Controls.Add(this.metroLabel2);
            this.tabPage4.Controls.Add(this.txtUser);
            this.tabPage4.Controls.Add(this.txtNvJoinDate);
            this.tabPage4.Controls.Add(this.txtNvBirthday);
            this.tabPage4.Controls.Add(this.txtNvRole);
            this.tabPage4.Controls.Add(this.txtNvUsername);
            this.tabPage4.Controls.Add(this.metroLabel9);
            this.tabPage4.Controls.Add(this.metroLabel7);
            this.tabPage4.Controls.Add(this.metroLabel5);
            this.tabPage4.Controls.Add(this.metroLabel3);
            this.tabPage4.Controls.Add(this.txtNvName);
            this.tabPage4.Controls.Add(this.metroLabel1);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.button5);
            this.tabPage4.Controls.Add(this.passUpdate);
            this.tabPage4.Controls.Add(this.repass);
            this.tabPage4.Controls.Add(this.newpass);
            this.tabPage4.Controls.Add(this.oldpass);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage4.Size = new System.Drawing.Size(1256, 415);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Tài khoản";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel6.Location = new System.Drawing.Point(713, 146);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(180, 25);
            this.metroLabel6.TabIndex = 26;
            this.metroLabel6.Text = "Nhập Lại Mật Khẩu:";
            this.metroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel4.Location = new System.Drawing.Point(713, 102);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(138, 25);
            this.metroLabel4.TabIndex = 25;
            this.metroLabel4.Text = "Mật Khẩu Mới:";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(713, 56);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(127, 25);
            this.metroLabel2.TabIndex = 24;
            this.metroLabel2.Text = "Mật Khẩu Cũ:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUser
            // 
            this.txtUser.AutoSize = true;
            this.txtUser.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.txtUser.Location = new System.Drawing.Point(339, 23);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(0, 0);
            this.txtUser.TabIndex = 23;
            // 
            // txtNvJoinDate
            // 
            this.txtNvJoinDate.AutoSize = true;
            this.txtNvJoinDate.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.txtNvJoinDate.Location = new System.Drawing.Point(215, 244);
            this.txtNvJoinDate.Name = "txtNvJoinDate";
            this.txtNvJoinDate.Size = new System.Drawing.Size(0, 0);
            this.txtNvJoinDate.TabIndex = 22;
            // 
            // txtNvBirthday
            // 
            this.txtNvBirthday.AutoSize = true;
            this.txtNvBirthday.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.txtNvBirthday.Location = new System.Drawing.Point(215, 197);
            this.txtNvBirthday.Name = "txtNvBirthday";
            this.txtNvBirthday.Size = new System.Drawing.Size(0, 0);
            this.txtNvBirthday.TabIndex = 21;
            // 
            // txtNvRole
            // 
            this.txtNvRole.AutoSize = true;
            this.txtNvRole.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.txtNvRole.Location = new System.Drawing.Point(215, 152);
            this.txtNvRole.Name = "txtNvRole";
            this.txtNvRole.Size = new System.Drawing.Size(0, 0);
            this.txtNvRole.TabIndex = 20;
            // 
            // txtNvUsername
            // 
            this.txtNvUsername.AutoSize = true;
            this.txtNvUsername.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.txtNvUsername.Location = new System.Drawing.Point(215, 108);
            this.txtNvUsername.Name = "txtNvUsername";
            this.txtNvUsername.Size = new System.Drawing.Size(0, 0);
            this.txtNvUsername.TabIndex = 19;
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel9.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel9.Location = new System.Drawing.Point(45, 238);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(139, 25);
            this.metroLabel9.TabIndex = 18;
            this.metroLabel9.Text = "Ngày Vào Làm:";
            this.metroLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel7.Location = new System.Drawing.Point(45, 191);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(104, 25);
            this.metroLabel7.TabIndex = 17;
            this.metroLabel7.Text = "Ngày Sinh:";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel5.Location = new System.Drawing.Point(45, 146);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(83, 25);
            this.metroLabel5.TabIndex = 16;
            this.metroLabel5.Text = "Chức Vụ";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel3.Location = new System.Drawing.Point(45, 102);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(101, 25);
            this.metroLabel3.TabIndex = 15;
            this.metroLabel3.Text = "Tài Khoản:";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNvName
            // 
            this.txtNvName.AutoSize = true;
            this.txtNvName.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.txtNvName.Location = new System.Drawing.Point(215, 62);
            this.txtNvName.Name = "txtNvName";
            this.txtNvName.Size = new System.Drawing.Size(0, 0);
            this.txtNvName.TabIndex = 6;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(45, 56);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(77, 25);
            this.metroLabel1.TabIndex = 5;
            this.metroLabel1.Text = "Họ Tên:";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label16.Location = new System.Drawing.Point(869, 17);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(148, 25);
            this.label16.TabIndex = 4;
            this.label16.Text = "Đổi Mật Khẩu";
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button5.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LimeGreen;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(1132, 356);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(120, 55);
            this.button5.TabIndex = 5;
            this.button5.Text = "Cấp tài khoản";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.Button5_Click);
            // 
            // passUpdate
            // 
            this.passUpdate.BackColor = System.Drawing.Color.DodgerBlue;
            this.passUpdate.Enabled = false;
            this.passUpdate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan;
            this.passUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.passUpdate.ForeColor = System.Drawing.Color.White;
            this.passUpdate.Location = new System.Drawing.Point(995, 212);
            this.passUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.passUpdate.Name = "passUpdate";
            this.passUpdate.Size = new System.Drawing.Size(95, 32);
            this.passUpdate.TabIndex = 4;
            this.passUpdate.Text = "Cập nhật";
            this.passUpdate.UseVisualStyleBackColor = false;
            this.passUpdate.Click += new System.EventHandler(this.passUpdate_Click);
            // 
            // repass
            // 
            this.repass.Location = new System.Drawing.Point(945, 146);
            this.repass.Margin = new System.Windows.Forms.Padding(2);
            this.repass.Name = "repass";
            this.repass.PasswordChar = '●';
            this.repass.Size = new System.Drawing.Size(145, 20);
            this.repass.TabIndex = 3;
            this.repass.KeyUp += new System.Windows.Forms.KeyEventHandler(this.repass_KeyUp);
            // 
            // newpass
            // 
            this.newpass.Location = new System.Drawing.Point(945, 102);
            this.newpass.Margin = new System.Windows.Forms.Padding(2);
            this.newpass.Name = "newpass";
            this.newpass.PasswordChar = '●';
            this.newpass.Size = new System.Drawing.Size(145, 20);
            this.newpass.TabIndex = 2;
            // 
            // oldpass
            // 
            this.oldpass.Location = new System.Drawing.Point(945, 56);
            this.oldpass.Margin = new System.Windows.Forms.Padding(2);
            this.oldpass.Name = "oldpass";
            this.oldpass.PasswordChar = '●';
            this.oldpass.Size = new System.Drawing.Size(145, 20);
            this.oldpass.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label15.Location = new System.Drawing.Point(176, 17);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(149, 25);
            this.label15.TabIndex = 0;
            this.label15.Text = "Thông tin Của";
            // 
            // btnSignOut
            // 
            this.btnSignOut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSignOut.BackColor = System.Drawing.Color.OrangeRed;
            this.btnSignOut.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.btnSignOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSignOut.ForeColor = System.Drawing.Color.White;
            this.btnSignOut.Location = new System.Drawing.Point(1176, 6);
            this.btnSignOut.Name = "btnSignOut";
            this.btnSignOut.Size = new System.Drawing.Size(75, 23);
            this.btnSignOut.TabIndex = 27;
            this.btnSignOut.Text = "Đăng Xuất";
            this.btnSignOut.UseVisualStyleBackColor = false;
            this.btnSignOut.Click += new System.EventHandler(this.btnSignOut_Click);
            // 
            // fAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WindowsFormsApp1.Properties.Resources.login;
            this.ClientSize = new System.Drawing.Size(1284, 461);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "fAdmin";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fAdmin";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txt_tongtientratruoc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dt_pickden;
        private System.Windows.Forms.DateTimePicker dt_picktu;
        private System.Windows.Forms.TextBox txt_tongtientrasau;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dt_picktrasauden;
        private System.Windows.Forms.DateTimePicker dt_picktrasautu;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox txt_manv;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dt_pickngayvaolam;
        private System.Windows.Forms.DateTimePicker dt_pickngaysinh;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txt_tennv;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RadioButton rd_nu;
        private System.Windows.Forms.RadioButton rd_nam;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button passUpdate;
        private System.Windows.Forms.TextBox repass;
        private System.Windows.Forms.TextBox newpass;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ListView lv_nhanvien;
        private System.Windows.Forms.ColumnHeader iMaNV;
        private System.Windows.Forms.ColumnHeader sTenNV;
        private System.Windows.Forms.ColumnHeader bGioitinh;
        private System.Windows.Forms.ColumnHeader dNgaysinh;
        private System.Windows.Forms.ColumnHeader dNgayvaolam;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btn_xem;
        private System.Windows.Forms.ListView lv_tratruoc;
        private System.Windows.Forms.ColumnHeader iMaHD;
        private System.Windows.Forms.ColumnHeader iMaThueBao;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader iMaLoai;
        private System.Windows.Forms.ColumnHeader sTenThueBao;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader sSDT;
        private System.Windows.Forms.ColumnHeader fTongTien;
        private System.Windows.Forms.ColumnHeader dNgaylap;
        private System.Windows.Forms.ColumnHeader fThoigiangoi;
        private System.Windows.Forms.ColumnHeader fTinnhangui;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btn_thongke;
        private System.Windows.Forms.Label VND;
        private System.Windows.Forms.ListView lv_tbtrasau;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.Label label16;
        private MetroFramework.Controls.MetroLabel txtNvName;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel txtNvUsername;
        private MetroFramework.Controls.MetroLabel txtNvRole;
        private MetroFramework.Controls.MetroLabel txtNvBirthday;
        private MetroFramework.Controls.MetroLabel txtNvJoinDate;
        private MetroFramework.Controls.MetroLabel txtUser;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.TextBox oldpass;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel lbNoData;
        private System.Windows.Forms.Button btnSignOut;
    }
}