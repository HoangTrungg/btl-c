﻿namespace WindowsFormsApp1
{
    partial class fDangnhap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txt_matkhau = new MetroFramework.Controls.MetroTextBox();
            this.txt_taikhoan = new MetroFramework.Controls.MetroTextBox();
            this.lb_matkhau = new System.Windows.Forms.Label();
            this.lb_messagetk = new System.Windows.Forms.Label();
            this.formExit = new System.Windows.Forms.Button();
            this.btn_dangnhap = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(600, 72);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft JhengHei", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(200, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(239, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "Quản lý thuê bao";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.txt_matkhau);
            this.panel2.Controls.Add(this.txt_taikhoan);
            this.panel2.Controls.Add(this.lb_matkhau);
            this.panel2.Controls.Add(this.lb_messagetk);
            this.panel2.Controls.Add(this.formExit);
            this.panel2.Controls.Add(this.btn_dangnhap);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 72);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(600, 428);
            this.panel2.TabIndex = 1;
            // 
            // txt_matkhau
            // 
            // 
            // 
            // 
            this.txt_matkhau.CustomButton.Image = null;
            this.txt_matkhau.CustomButton.Location = new System.Drawing.Point(127, 2);
            this.txt_matkhau.CustomButton.Name = "";
            this.txt_matkhau.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.txt_matkhau.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txt_matkhau.CustomButton.TabIndex = 1;
            this.txt_matkhau.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txt_matkhau.CustomButton.UseSelectable = true;
            this.txt_matkhau.CustomButton.Visible = false;
            this.txt_matkhau.DisplayIcon = true;
            this.txt_matkhau.Icon = global::WindowsFormsApp1.Properties.Resources.padlock;
            this.txt_matkhau.Lines = new string[0];
            this.txt_matkhau.Location = new System.Drawing.Point(257, 183);
            this.txt_matkhau.MaxLength = 32767;
            this.txt_matkhau.Name = "txt_matkhau";
            this.txt_matkhau.PasswordChar = '●';
            this.txt_matkhau.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_matkhau.SelectedText = "";
            this.txt_matkhau.SelectionLength = 0;
            this.txt_matkhau.SelectionStart = 0;
            this.txt_matkhau.ShortcutsEnabled = true;
            this.txt_matkhau.Size = new System.Drawing.Size(149, 24);
            this.txt_matkhau.TabIndex = 1;
            this.txt_matkhau.UseSelectable = true;
            this.txt_matkhau.UseSystemPasswordChar = true;
            this.txt_matkhau.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txt_matkhau.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txt_matkhau.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_matkhau_KeyUp);
            // 
            // txt_taikhoan
            // 
            // 
            // 
            // 
            this.txt_taikhoan.CustomButton.Image = null;
            this.txt_taikhoan.CustomButton.Location = new System.Drawing.Point(127, 2);
            this.txt_taikhoan.CustomButton.Name = "";
            this.txt_taikhoan.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.txt_taikhoan.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txt_taikhoan.CustomButton.TabIndex = 1;
            this.txt_taikhoan.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txt_taikhoan.CustomButton.UseSelectable = true;
            this.txt_taikhoan.CustomButton.Visible = false;
            this.txt_taikhoan.DisplayIcon = true;
            this.txt_taikhoan.Icon = global::WindowsFormsApp1.Properties.Resources.user;
            this.txt_taikhoan.Lines = new string[0];
            this.txt_taikhoan.Location = new System.Drawing.Point(257, 134);
            this.txt_taikhoan.MaxLength = 32767;
            this.txt_taikhoan.Name = "txt_taikhoan";
            this.txt_taikhoan.PasswordChar = '\0';
            this.txt_taikhoan.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_taikhoan.SelectedText = "";
            this.txt_taikhoan.SelectionLength = 0;
            this.txt_taikhoan.SelectionStart = 0;
            this.txt_taikhoan.ShortcutsEnabled = true;
            this.txt_taikhoan.Size = new System.Drawing.Size(149, 24);
            this.txt_taikhoan.TabIndex = 0;
            this.txt_taikhoan.UseSelectable = true;
            this.txt_taikhoan.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txt_taikhoan.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lb_matkhau
            // 
            this.lb_matkhau.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_matkhau.ForeColor = System.Drawing.Color.OrangeRed;
            this.lb_matkhau.Location = new System.Drawing.Point(410, 190);
            this.lb_matkhau.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lb_matkhau.Name = "lb_matkhau";
            this.lb_matkhau.Size = new System.Drawing.Size(181, 19);
            this.lb_matkhau.TabIndex = 5;
            this.lb_matkhau.Text = " lb";
            this.lb_matkhau.Visible = false;
            // 
            // lb_messagetk
            // 
            this.lb_messagetk.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_messagetk.ForeColor = System.Drawing.Color.OrangeRed;
            this.lb_messagetk.Location = new System.Drawing.Point(410, 141);
            this.lb_messagetk.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lb_messagetk.Name = "lb_messagetk";
            this.lb_messagetk.Size = new System.Drawing.Size(181, 19);
            this.lb_messagetk.TabIndex = 5;
            this.lb_messagetk.Text = " lb";
            this.lb_messagetk.Visible = false;
            // 
            // formExit
            // 
            this.formExit.BackColor = System.Drawing.Color.Crimson;
            this.formExit.Font = new System.Drawing.Font("Times New Roman", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formExit.ForeColor = System.Drawing.Color.White;
            this.formExit.Location = new System.Drawing.Point(333, 235);
            this.formExit.Margin = new System.Windows.Forms.Padding(2);
            this.formExit.Name = "formExit";
            this.formExit.Size = new System.Drawing.Size(72, 33);
            this.formExit.TabIndex = 3;
            this.formExit.Text = "Thoát";
            this.formExit.UseVisualStyleBackColor = false;
            this.formExit.Click += new System.EventHandler(this.formExit_Click);
            // 
            // btn_dangnhap
            // 
            this.btn_dangnhap.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_dangnhap.Font = new System.Drawing.Font("Times New Roman", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_dangnhap.ForeColor = System.Drawing.Color.White;
            this.btn_dangnhap.Location = new System.Drawing.Point(256, 235);
            this.btn_dangnhap.Margin = new System.Windows.Forms.Padding(2);
            this.btn_dangnhap.Name = "btn_dangnhap";
            this.btn_dangnhap.Size = new System.Drawing.Size(72, 33);
            this.btn_dangnhap.TabIndex = 2;
            this.btn_dangnhap.Text = "Đăng nhập";
            this.btn_dangnhap.UseVisualStyleBackColor = false;
            this.btn_dangnhap.Click += new System.EventHandler(this.Btn_dangnhap_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(172, 139);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tài khoản";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(173, 188);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 19);
            this.label3.TabIndex = 0;
            this.label3.Text = "Mật khẩu";
            // 
            // fDangnhap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackgroundImage = global::WindowsFormsApp1.Properties.Resources.login;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(600, 500);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "fDangnhap";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng Nhập";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_dangnhap;
        private System.Windows.Forms.Button formExit;
        private System.Windows.Forms.Label lb_matkhau;
        private System.Windows.Forms.Label lb_messagetk;
        private MetroFramework.Controls.MetroTextBox txt_taikhoan;
        private MetroFramework.Controls.MetroTextBox txt_matkhau;
    }
}

