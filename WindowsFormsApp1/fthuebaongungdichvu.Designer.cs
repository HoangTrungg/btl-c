﻿namespace WindowsFormsApp1
{
    partial class fthuebaongungdichvu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_loaithuebao = new System.Windows.Forms.TextBox();
            this.dt_pickngaydangky = new System.Windows.Forms.DateTimePicker();
            this.rd_nu = new System.Windows.Forms.RadioButton();
            this.rd_Nam = new System.Windows.Forms.RadioButton();
            this.txt_CMND = new System.Windows.Forms.TextBox();
            this.txt_sdt = new System.Windows.Forms.TextBox();
            this.txt_tuoi = new System.Windows.Forms.TextBox();
            this.txt_tenthuebao = new System.Windows.Forms.TextBox();
            this.txt_mathuebao = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lv_tbtrasauhuydichvu = new System.Windows.Forms.ListView();
            this.iMaThueBao = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.iMaLoai = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sTenThueBao = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bGioitinh = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sSDT = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.iTuoi = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sCMND = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dNgayDangKy = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btn_capdv = new System.Windows.Forms.Button();
            this.btn_thuebaotrasau = new System.Windows.Forms.Button();
            this.btn_thuebaotratruoc = new System.Windows.Forms.Button();
            this.lb_thuebao = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_loaithuebao
            // 
            this.txt_loaithuebao.Enabled = false;
            this.txt_loaithuebao.Location = new System.Drawing.Point(83, 370);
            this.txt_loaithuebao.Margin = new System.Windows.Forms.Padding(2);
            this.txt_loaithuebao.Name = "txt_loaithuebao";
            this.txt_loaithuebao.Size = new System.Drawing.Size(110, 20);
            this.txt_loaithuebao.TabIndex = 2;
            // 
            // dt_pickngaydangky
            // 
            this.dt_pickngaydangky.Enabled = false;
            this.dt_pickngaydangky.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_pickngaydangky.Location = new System.Drawing.Point(521, 370);
            this.dt_pickngaydangky.Margin = new System.Windows.Forms.Padding(2);
            this.dt_pickngaydangky.Name = "dt_pickngaydangky";
            this.dt_pickngaydangky.Size = new System.Drawing.Size(91, 20);
            this.dt_pickngaydangky.TabIndex = 9;
            // 
            // rd_nu
            // 
            this.rd_nu.AutoSize = true;
            this.rd_nu.Enabled = false;
            this.rd_nu.Location = new System.Drawing.Point(569, 330);
            this.rd_nu.Margin = new System.Windows.Forms.Padding(2);
            this.rd_nu.Name = "rd_nu";
            this.rd_nu.Size = new System.Drawing.Size(39, 17);
            this.rd_nu.TabIndex = 8;
            this.rd_nu.Text = "Nữ";
            this.rd_nu.UseVisualStyleBackColor = true;
            // 
            // rd_Nam
            // 
            this.rd_Nam.AutoSize = true;
            this.rd_Nam.Checked = true;
            this.rd_Nam.Enabled = false;
            this.rd_Nam.Location = new System.Drawing.Point(521, 330);
            this.rd_Nam.Margin = new System.Windows.Forms.Padding(2);
            this.rd_Nam.Name = "rd_Nam";
            this.rd_Nam.Size = new System.Drawing.Size(47, 17);
            this.rd_Nam.TabIndex = 7;
            this.rd_Nam.TabStop = true;
            this.rd_Nam.Text = "Nam";
            this.rd_Nam.UseVisualStyleBackColor = true;
            // 
            // txt_CMND
            // 
            this.txt_CMND.Enabled = false;
            this.txt_CMND.Location = new System.Drawing.Point(300, 370);
            this.txt_CMND.Margin = new System.Windows.Forms.Padding(2);
            this.txt_CMND.Name = "txt_CMND";
            this.txt_CMND.Size = new System.Drawing.Size(110, 20);
            this.txt_CMND.TabIndex = 5;
            this.txt_CMND.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_CMND_KeyPress);
            // 
            // txt_sdt
            // 
            this.txt_sdt.Enabled = false;
            this.txt_sdt.Location = new System.Drawing.Point(300, 332);
            this.txt_sdt.Margin = new System.Windows.Forms.Padding(2);
            this.txt_sdt.Name = "txt_sdt";
            this.txt_sdt.Size = new System.Drawing.Size(110, 20);
            this.txt_sdt.TabIndex = 4;
            this.txt_sdt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_sdt_KeyPress);
            // 
            // txt_tuoi
            // 
            this.txt_tuoi.Enabled = false;
            this.txt_tuoi.Location = new System.Drawing.Point(300, 410);
            this.txt_tuoi.Margin = new System.Windows.Forms.Padding(2);
            this.txt_tuoi.Name = "txt_tuoi";
            this.txt_tuoi.Size = new System.Drawing.Size(110, 20);
            this.txt_tuoi.TabIndex = 6;
            // 
            // txt_tenthuebao
            // 
            this.txt_tenthuebao.Enabled = false;
            this.txt_tenthuebao.Location = new System.Drawing.Point(83, 410);
            this.txt_tenthuebao.Margin = new System.Windows.Forms.Padding(2);
            this.txt_tenthuebao.Name = "txt_tenthuebao";
            this.txt_tenthuebao.Size = new System.Drawing.Size(110, 20);
            this.txt_tenthuebao.TabIndex = 3;
            // 
            // txt_mathuebao
            // 
            this.txt_mathuebao.Enabled = false;
            this.txt_mathuebao.Location = new System.Drawing.Point(83, 332);
            this.txt_mathuebao.Margin = new System.Windows.Forms.Padding(2);
            this.txt_mathuebao.Name = "txt_mathuebao";
            this.txt_mathuebao.Size = new System.Drawing.Size(110, 20);
            this.txt_mathuebao.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(227, 413);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Tuổi";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(229, 303);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Số điện thoại";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 413);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Tên thuê bao";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(445, 373);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Ngày đăng ký";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(445, 332);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Giới tính";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(227, 372);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "CMND";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 372);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Loại thuê bao";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 334);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Mã thuê bao";
            // 
            // lv_tbtrasauhuydichvu
            // 
            this.lv_tbtrasauhuydichvu.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.iMaThueBao,
            this.iMaLoai,
            this.sTenThueBao,
            this.bGioitinh,
            this.sSDT,
            this.iTuoi,
            this.sCMND,
            this.dNgayDangKy});
            this.lv_tbtrasauhuydichvu.FullRowSelect = true;
            this.lv_tbtrasauhuydichvu.Location = new System.Drawing.Point(9, 62);
            this.lv_tbtrasauhuydichvu.Margin = new System.Windows.Forms.Padding(2);
            this.lv_tbtrasauhuydichvu.Name = "lv_tbtrasauhuydichvu";
            this.lv_tbtrasauhuydichvu.Size = new System.Drawing.Size(980, 260);
            this.lv_tbtrasauhuydichvu.TabIndex = 30;
            this.lv_tbtrasauhuydichvu.UseCompatibleStateImageBehavior = false;
            this.lv_tbtrasauhuydichvu.View = System.Windows.Forms.View.Details;
            this.lv_tbtrasauhuydichvu.SelectedIndexChanged += new System.EventHandler(this.Lv_tbtrasauhuydichvu_SelectedIndexChanged);
            // 
            // iMaThueBao
            // 
            this.iMaThueBao.Text = "Mã thuê bao";
            this.iMaThueBao.Width = 109;
            // 
            // iMaLoai
            // 
            this.iMaLoai.Text = "Loại thuê bao";
            this.iMaLoai.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iMaLoai.Width = 133;
            // 
            // sTenThueBao
            // 
            this.sTenThueBao.Text = "Tên";
            this.sTenThueBao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sTenThueBao.Width = 186;
            // 
            // bGioitinh
            // 
            this.bGioitinh.DisplayIndex = 4;
            this.bGioitinh.Text = "Giới tính";
            this.bGioitinh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.bGioitinh.Width = 84;
            // 
            // sSDT
            // 
            this.sSDT.DisplayIndex = 3;
            this.sSDT.Text = "SDT";
            this.sSDT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sSDT.Width = 134;
            // 
            // iTuoi
            // 
            this.iTuoi.Text = "Tuổi";
            this.iTuoi.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iTuoi.Width = 85;
            // 
            // sCMND
            // 
            this.sCMND.Text = "CMND";
            this.sCMND.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sCMND.Width = 122;
            // 
            // dNgayDangKy
            // 
            this.dNgayDangKy.Text = "Ngày đăng ký";
            this.dNgayDangKy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dNgayDangKy.Width = 116;
            // 
            // btn_capdv
            // 
            this.btn_capdv.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_capdv.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btn_capdv.ForeColor = System.Drawing.Color.White;
            this.btn_capdv.Location = new System.Drawing.Point(483, 413);
            this.btn_capdv.Margin = new System.Windows.Forms.Padding(2);
            this.btn_capdv.Name = "btn_capdv";
            this.btn_capdv.Size = new System.Drawing.Size(129, 38);
            this.btn_capdv.TabIndex = 10;
            this.btn_capdv.Text = "Cấp lại dịch vụ";
            this.btn_capdv.UseVisualStyleBackColor = false;
            this.btn_capdv.Click += new System.EventHandler(this.Btn_capdv_Click);
            // 
            // btn_thuebaotrasau
            // 
            this.btn_thuebaotrasau.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_thuebaotrasau.BackColor = System.Drawing.Color.OrangeRed;
            this.btn_thuebaotrasau.Enabled = false;
            this.btn_thuebaotrasau.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.btn_thuebaotrasau.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_thuebaotrasau.ForeColor = System.Drawing.Color.White;
            this.btn_thuebaotrasau.Location = new System.Drawing.Point(860, 501);
            this.btn_thuebaotrasau.Margin = new System.Windows.Forms.Padding(2);
            this.btn_thuebaotrasau.Name = "btn_thuebaotrasau";
            this.btn_thuebaotrasau.Size = new System.Drawing.Size(129, 38);
            this.btn_thuebaotrasau.TabIndex = 32;
            this.btn_thuebaotrasau.Text = "Thuê bao trả sau";
            this.btn_thuebaotrasau.UseVisualStyleBackColor = false;
            this.btn_thuebaotrasau.Click += new System.EventHandler(this.Btn_thuebaotrasau_Click);
            // 
            // btn_thuebaotratruoc
            // 
            this.btn_thuebaotratruoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_thuebaotratruoc.BackColor = System.Drawing.Color.OrangeRed;
            this.btn_thuebaotratruoc.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.btn_thuebaotratruoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_thuebaotratruoc.ForeColor = System.Drawing.Color.White;
            this.btn_thuebaotratruoc.Location = new System.Drawing.Point(860, 459);
            this.btn_thuebaotratruoc.Margin = new System.Windows.Forms.Padding(2);
            this.btn_thuebaotratruoc.Name = "btn_thuebaotratruoc";
            this.btn_thuebaotratruoc.Size = new System.Drawing.Size(129, 38);
            this.btn_thuebaotratruoc.TabIndex = 32;
            this.btn_thuebaotratruoc.Text = "Thuê bao trả trước";
            this.btn_thuebaotratruoc.UseVisualStyleBackColor = false;
            this.btn_thuebaotratruoc.Click += new System.EventHandler(this.Btn_thuebaotratruoc_Click);
            // 
            // lb_thuebao
            // 
            this.lb_thuebao.AutoSize = true;
            this.lb_thuebao.Font = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_thuebao.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lb_thuebao.Location = new System.Drawing.Point(329, 24);
            this.lb_thuebao.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lb_thuebao.Name = "lb_thuebao";
            this.lb_thuebao.Size = new System.Drawing.Size(355, 36);
            this.lb_thuebao.TabIndex = 33;
            this.lb_thuebao.Text = "Thuê bao đã ngưng dịch vụ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(227, 335);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 38;
            this.label1.Text = "SĐT";
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBack.BackColor = System.Drawing.Color.Red;
            this.btnBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkRed;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(11, 498);
            this.btnBack.Margin = new System.Windows.Forms.Padding(2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(78, 41);
            this.btnBack.TabIndex = 39;
            this.btnBack.Text = "Trở Về";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // fthuebaongungdichvu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 550);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lb_thuebao);
            this.Controls.Add(this.btn_thuebaotratruoc);
            this.Controls.Add(this.btn_thuebaotrasau);
            this.Controls.Add(this.btn_capdv);
            this.Controls.Add(this.lv_tbtrasauhuydichvu);
            this.Controls.Add(this.txt_loaithuebao);
            this.Controls.Add(this.dt_pickngaydangky);
            this.Controls.Add(this.rd_nu);
            this.Controls.Add(this.rd_Nam);
            this.Controls.Add(this.txt_CMND);
            this.Controls.Add(this.txt_sdt);
            this.Controls.Add(this.txt_tuoi);
            this.Controls.Add(this.txt_tenthuebao);
            this.Controls.Add(this.txt_mathuebao);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "fthuebaongungdichvu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fthuebaongungdichvu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_loaithuebao;
        private System.Windows.Forms.DateTimePicker dt_pickngaydangky;
        private System.Windows.Forms.RadioButton rd_nu;
        private System.Windows.Forms.RadioButton rd_Nam;
        private System.Windows.Forms.TextBox txt_CMND;
        private System.Windows.Forms.TextBox txt_sdt;
        private System.Windows.Forms.TextBox txt_tuoi;
        private System.Windows.Forms.TextBox txt_tenthuebao;
        private System.Windows.Forms.TextBox txt_mathuebao;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView lv_tbtrasauhuydichvu;
        private System.Windows.Forms.ColumnHeader iMaThueBao;
        private System.Windows.Forms.ColumnHeader iMaLoai;
        private System.Windows.Forms.ColumnHeader sTenThueBao;
        private System.Windows.Forms.ColumnHeader bGioitinh;
        private System.Windows.Forms.ColumnHeader sSDT;
        private System.Windows.Forms.ColumnHeader iTuoi;
        private System.Windows.Forms.ColumnHeader sCMND;
        private System.Windows.Forms.ColumnHeader dNgayDangKy;
        private System.Windows.Forms.Button btn_capdv;
        private System.Windows.Forms.Button btn_thuebaotrasau;
        private System.Windows.Forms.Button btn_thuebaotratruoc;
        private System.Windows.Forms.Label lb_thuebao;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBack;
    }
}