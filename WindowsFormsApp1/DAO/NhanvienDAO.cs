﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.DTO;

namespace WindowsFormsApp1.DAO
{
    class NhanvienDAO
    {
        DataProvider provider = new DataProvider();
        private static NhanvienDAO instance;
        public static NhanvienDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new NhanvienDAO();
                }
                return instance;
            }
        }
        public List<NhanVienDTO> getdata(object[] parameter, List<string> listparam)
        {
            List<NhanVienDTO> listnv = new List<NhanVienDTO>();
            DataTable dt = provider.ExcuteQuery("sp_getdataNV",parameter,listparam);
            if(dt != null && dt.Rows.Count > 0)
            {
                NhanVienDTO nv;
                foreach(DataRow row in dt.Rows)
                {
                    //public NhanVienDTO(int iMaNV, string sTenNV, bool bGioitinh, DateTime dNgaysinh, DateTime dNgayvaolam)
                    int iMaNV = int.Parse(row["iMaNV"].ToString());
                    string sTenNV = row["sTenNV"].ToString();
                    bool bGioitinh = bool.Parse(row["bGioitinh"].ToString());
                    DateTime dNgaysinh = Convert.ToDateTime(row["dNgaysinh"].ToString());
                    DateTime dNgayvaolam = Convert.ToDateTime(row["dNgayvaolam"].ToString());
                    nv = new NhanVienDTO(iMaNV,sTenNV,bGioitinh,dNgaysinh,dNgayvaolam);
                    listnv.Add(nv);
                }
            }
            return listnv;

        }
        public bool insert(object[] parameter, List<string> listpara)
        {
            int result = provider.ExcuteNoneQuery("sp_themnv", parameter, listpara);
            if (result != 0)
            {
                return true;

            }
            else
                return false;
        }
        public bool update(object[] parameter, List<string> listpara)
        {
            int result = provider.ExcuteNoneQuery("sp_updatenv", parameter, listpara);
            if (result != 0)
            {
                return true;

            }
            else
                return false;
        }
        public bool updatetrangthainghiviec(object[] parameter, List<string> listpara)
        {
            int result = provider.ExcuteNoneQuery("sp_nghiviec", parameter, listpara);
            if (result != 0)
            {
                return true;

            }
            else
                return false;
        }
    }
}
