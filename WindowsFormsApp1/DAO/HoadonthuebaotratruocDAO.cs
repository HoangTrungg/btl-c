﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.DTO;

namespace WindowsFormsApp1.DAO
{
    class HoadonthuebaotratruocDAO
    {
        DataProvider provider = new DataProvider();
        private static HoadonthuebaotratruocDAO instance;
        public static HoadonthuebaotratruocDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new HoadonthuebaotratruocDAO();
                }
                return instance;
            }
        }
        public List<HoadonthuebaotratruocDTO> getdata(object[] parameter, List<string> listparam)
        {
            List<HoadonthuebaotratruocDTO> listtb_hoadontratruoc = new List<HoadonthuebaotratruocDTO>();
            DataTable dt = provider.ExcuteQuery("sp_hoadonthuebaotratruoc", parameter, listparam);
            if (dt != null && dt.Rows.Count > 0)
            {
                HoadonthuebaotratruocDTO hoadon_tbtratruoc;
                foreach (DataRow row in dt.Rows)
                {
                    int iMaHD = int.Parse(row["iMaHD"].ToString());
                    int iMaThueBao = int.Parse(row["iMaThueBao"].ToString());
                    int iMaNV = int.Parse(row["iMaNV"].ToString());
                    int iMaLoai = int.Parse(row["iMaLoai"].ToString());
                    string sTenThueBao = row["sTenThueBao"].ToString();
                    string sSDT = row["sSDT"].ToString();
                    bool bGioitinh = bool.Parse(row["bGioitinh"].ToString());
                    int fThoigiangoi = int.Parse(row["fThoiGianGoi"].ToString());
                    int fTinnhangui = int.Parse(row["iTinNhanGui"].ToString());
                    DateTime dNgaylap = Convert.ToDateTime(row["dNgayLap"].ToString());
                    float fTongTien = float.Parse(row["fTongTien"].ToString());
                    hoadon_tbtratruoc = new HoadonthuebaotratruocDTO(iMaHD,iMaThueBao,iMaNV,iMaLoai,sTenThueBao,bGioitinh,sSDT,fTongTien,dNgaylap,fThoigiangoi,fTinnhangui);
                    listtb_hoadontratruoc.Add(hoadon_tbtratruoc);
                }
            }
            return listtb_hoadontratruoc;

        }
        public List<HoadonthuebaotratruocDTO> getdatatketheongay(object[] parameter, List<string> listparam)
        {
            List<HoadonthuebaotratruocDTO> listtb_hoadontratruoc = new List<HoadonthuebaotratruocDTO>();
            DataTable dt = provider.ExcuteQuery("sp_thongketheongay", parameter, listparam);
            if (dt != null && dt.Rows.Count > 0)
            {
                HoadonthuebaotratruocDTO hoadon_tbtratruoc;
                foreach (DataRow row in dt.Rows)
                {
                    int iMaHD = int.Parse(row["iMaHD"].ToString());
                    int iMaThueBao = int.Parse(row["iMaThueBao"].ToString());
                    int iMaNV = int.Parse(row["iMaNV"].ToString());
                    int iMaLoai = int.Parse(row["iMaLoai"].ToString());
                    string sTenThueBao = row["sTenThueBao"].ToString();
                    string sSDT = row["sSDT"].ToString();
                    bool bGioitinh = bool.Parse(row["bGioitinh"].ToString());
                    int fThoigiangoi = int.Parse(row["fThoiGianGoi"].ToString());
                    int fTinnhangui = int.Parse(row["iTinNhanGui"].ToString());
                    DateTime dNgaylap = Convert.ToDateTime(row["dNgayLap"].ToString());
                    float fTongTien = float.Parse(row["fTongTien"].ToString());
                    hoadon_tbtratruoc = new HoadonthuebaotratruocDTO(iMaHD, iMaThueBao, iMaNV, iMaLoai, sTenThueBao, bGioitinh, sSDT, fTongTien, dNgaylap, fThoigiangoi, fTinnhangui);
                    listtb_hoadontratruoc.Add(hoadon_tbtratruoc);
                }
            }
            return listtb_hoadontratruoc;

        }
        //public List<HoadonthuebaotratruocDTO> gettong(object[] parameter, List<string> listparameter)
        //{
        //    List<HoadonthuebaotratruocDTO> list_tbtratruoc = new List<HoadonthuebaotratruocDTO>();
        //    DataTable dt = provider.ExcuteQuery("sp_tong", parameter, listparameter);
        //    if(dt !=null && dt.Rows.Count > 0)
        //    {
        //        HoadonthuebaotratruocDTO hoadon_tbtratruoc;
        //        foreach(DataRow row in dt.Rows)
        //        {
        //            float tongtien = float.Parse(row["fTongtientatcathuebao"].ToString());
        //            hoadon_tbtratruoc = new HoadonthuebaotratruocDTO(tongtien);
        //            list_tbtratruoc.Add(hoadon_tbtratruoc);
        //        }
        //    }
        //    return list_tbtratruoc;
        //}


        public string gettong(object[] parameter, List<string> listparameter)
        {
            string tong ="";
            List<HoadonthuebaotratruocDTO> list_tbtratruoc = new List<HoadonthuebaotratruocDTO>();
            DataTable dt = provider.ExcuteQuery("sp_tong", parameter, listparameter);
            if (dt != null && dt.Rows.Count > 0)
            {
                tong = (dt.Rows[0][0].ToString());
                
            }
            return tong;
        }
    }
}
