﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.DAO
{
    
    class DataProvider
    {
        //string connection = @"Data Source=Sumanucians;Initial Catalog=db_thuebao;Integrated Security=True";
        string connection = @"Data Source=ANEX\SQLEXPRESS;Initial Catalog=db_thuebao;Integrated Security=True";

        /*private static DataProvider instance;
        public static DataProvider Instance
        {
            get
            {
                if (instance == null)
                    instance = new DataProvider();
                return instance;
            }
            private set { instance = value; }
        }
        private DataProvider() { }
        */
        public SqlConnection cn = new SqlConnection();

        public void kn_csdl()
        {
            cn.ConnectionString = connection;
            cn.Open();
        }
        public string lay1giatri(string query)
        {
            string kq = "";
            try
            {
                kn_csdl();
                SqlCommand command = new SqlCommand(query, cn);
                kq = command.ExecuteScalar().ToString();

            }
            catch
            {
                kq = null;
            }
            finally
            {
                dong_kn();
            }
            return kq;
        }

        private void dong_kn()
        {
            if(cn.State == ConnectionState.Open)
            {
                cn.Close();
            }
        }
        public DataTable gettable(string query)
        {
            DataTable table = new DataTable();
            try
            {
                kn_csdl();
                SqlDataAdapter adapter = new SqlDataAdapter(query, cn);
                DataSet ds = new DataSet();
                adapter.Fill(table);

            }
            catch (System.Exception)
            {
                table = null;
            }
            finally
            {
                dong_kn();
            }
            return table;
        }

        public int xulydulieu(string query)
        {
            int kq = 0;
            try
            {
                kn_csdl();
                SqlCommand lenh = new SqlCommand(query, cn);
                kq = lenh.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //Thông báo lỗi ra!
                kq = 0;
                throw ex;
            }
            finally
            {
                dong_kn();
            }
            return kq;
        }
        public DataTable ExcuteQuery(string procname, object[] parameter = null, List<string> listpara = null )
        {
            
            using(SqlConnection cnn = new SqlConnection(connection))
            {
                DataTable dt = new DataTable();
                try
                {
                    cnn.Open();
                    SqlCommand command = new SqlCommand(procname, cnn);
                    command.CommandType = CommandType.StoredProcedure;
                    if(parameter != null)
                    {
                        for(int i=0; i<parameter.Length; i++)
                        {
                            command.Parameters.AddWithValue(listpara[i], parameter[i]);
                        }
                    }
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dt);
                    cnn.Close();
                }
                catch (Exception ex)
                {
                    dt = null;
                    throw ex;
                }

                return dt;  
            }

        }
        public int ExcuteNoneQuery(string procname, object[] parameter = null, List<string> listpara = null)
        {
            using(SqlConnection cnn = new SqlConnection(connection))
            {
                int kq = 0;
                try
                {
                    cnn.Open();
                    DataTable dt = new DataTable();
                    SqlCommand command = new SqlCommand(procname, cnn);
                    command.CommandType = CommandType.StoredProcedure;
                    if (parameter != null)
                    {
                        for(int i=0; i< parameter.Length; i++)
                        {
                            command.Parameters.AddWithValue(listpara[i], parameter[i]);
                        }
                    }
                    kq = command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    kq = 0;
                    throw ex;
                }
                return kq;
            }
        }
        public DataTable ExcuteDataReader(string procname, object[] parameter = null, List<string> listpara = null)
        {

            using(SqlConnection cnn = new SqlConnection(connection))
            {
                cnn.Open();
                DataTable dt = new DataTable();
                SqlCommand command = new SqlCommand(procname, cnn);
                command.CommandType = CommandType.StoredProcedure;
                if (parameter != null)
                {
                    for (int i = 0; i < parameter.Length; i++)
                    {
                        command.Parameters.AddWithValue(listpara[i], parameter[i]);
                    }
                }
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();

                }
                return dt;
            }
        }
    }
}
