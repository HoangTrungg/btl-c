﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.DTO;

namespace WindowsFormsApp1.DAO
{
    class TrasauDAO
    {
        DataProvider provider = new DataProvider();
        private static TrasauDAO instance;
        public static TrasauDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TrasauDAO();
                }
                return instance;
            }
        }
        public List<TrasauDTO> getdata(object[] parameter, List<string> listparam)
        {
            List<TrasauDTO> listtb_trasau = new List<TrasauDTO>();
            DataTable dt = provider.ExcuteQuery("sp_thuebaotrasau", parameter, listparam);
            if (dt != null && dt.Rows.Count > 0)
            {
                TrasauDTO tbtrasau;
                foreach (DataRow row in dt.Rows)
                {
                    
                    int iMaThueBao = int.Parse(row["iMaThueBao"].ToString());
                    int iMaLoai = int.Parse(row["iMaLoai"].ToString());
                    string sTenThueBao = row["sTenThueBao"].ToString();
                    string sSDT = row["sSDT"].ToString();
                    bool bGioitinh = bool.Parse(row["bGioitinh"].ToString());
                    int iTuoi = int.Parse(row["iTuoi"].ToString());
                    string sCMND = row["sCMND"].ToString();
                    DateTime dDangky = Convert.ToDateTime(row["dNgayDangKy"].ToString());
                    float fTongTien = float.Parse(row["fTongTien"].ToString());
                    tbtrasau = new TrasauDTO(iMaThueBao, iMaLoai, sTenThueBao, bGioitinh, sSDT, iTuoi, sCMND, dDangky, fTongTien);
                    listtb_trasau.Add(tbtrasau);
                }
            }
            return listtb_trasau;

        }
        public List<TrasaunhanvienDTO> getdatanv(object[] parameter, List<string> listparam)
        {
            List<TrasaunhanvienDTO> listtb_trasau = new List<TrasaunhanvienDTO>();
            DataTable dt = provider.ExcuteQuery("sp_thuebaotrasaunv", parameter, listparam);
            if (dt != null && dt.Rows.Count > 0)
            {
                TrasaunhanvienDTO tbtrasau;
                foreach (DataRow row in dt.Rows)
                {

                    int iMaThueBao = int.Parse(row["iMaThueBao"].ToString());
                    int iMaLoai = int.Parse(row["iMaLoai"].ToString());
                    string sTenThueBao = row["sTenThueBao"].ToString();
                    string sSDT = row["sSDT"].ToString();
                    bool bGioitinh = bool.Parse(row["bGioitinh"].ToString());
                    int iTuoi = int.Parse(row["iTuoi"].ToString());
                    string sCMND = row["sCMND"].ToString();
                    DateTime dDangky = Convert.ToDateTime(row["dNgayDangKy"].ToString());
                    tbtrasau = new TrasaunhanvienDTO(iMaThueBao, iMaLoai, sTenThueBao, bGioitinh, sSDT, iTuoi, sCMND, dDangky);
                    listtb_trasau.Add(tbtrasau);
                }
            }
            return listtb_trasau;

        }
        public List<TrasaunhanvienDTO> getdatathuebaongunfdv(object[] parameter, List<string> listparam)
        {
            List<TrasaunhanvienDTO> listtb_trasau = new List<TrasaunhanvienDTO>();
            DataTable dt = provider.ExcuteQuery("sp_thuebaotrasauhuydvnv", parameter, listparam);
            if (dt != null && dt.Rows.Count > 0)
            {
                TrasaunhanvienDTO tbtrasau;
                foreach (DataRow row in dt.Rows)
                {

                    int iMaThueBao = int.Parse(row["iMaThueBao"].ToString());
                    int iMaLoai = int.Parse(row["iMaLoai"].ToString());
                    string sTenThueBao = row["sTenThueBao"].ToString();
                    string sSDT = row["sSDT"].ToString();
                    bool bGioitinh = bool.Parse(row["bGioitinh"].ToString());
                    int iTuoi = int.Parse(row["iTuoi"].ToString());
                    string sCMND = row["sCMND"].ToString();
                    DateTime dDangky = Convert.ToDateTime(row["dNgayDangKy"].ToString());
                    tbtrasau = new TrasaunhanvienDTO(iMaThueBao, iMaLoai, sTenThueBao, bGioitinh, sSDT, iTuoi, sCMND, dDangky);
                    listtb_trasau.Add(tbtrasau);
                }
            }
            return listtb_trasau;

        }
        public bool insert(object[] parameter, List<string> listpara)
        {
            int result = provider.ExcuteNoneQuery("sp_themthuebaotrasau", parameter, listpara);
            if (result != 0)
            {
                return true;

            }
            else
                return false;
        }
        public bool update(object[] parameter, List<string> listpara)
        {
            int result = provider.ExcuteNoneQuery("sp_updatethuebaotrasau", parameter, listpara);
            if (result != 0)
            {
                return true;
            }
            else
                return false;
        }
        public bool updatetrangthai(object[] parameter, List<string> listpara)
        {
            int result = provider.ExcuteNoneQuery("sp_ngungdv", parameter, listpara);
            if (result != 0)
            {
                return true;
            }
            else
                return false;
        }
    }
}
