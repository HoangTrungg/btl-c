﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.DTO;

namespace WindowsFormsApp1.DAO
{
    class HoadonthuebaotrasauDAO
    {
        DataProvider provider = new DataProvider();
        private static HoadonthuebaotrasauDAO instance;
        public static HoadonthuebaotrasauDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new HoadonthuebaotrasauDAO();
                }
                return instance;
            }
        }
        public List<HoadonthuebaotrasauDTO> getdata(object[] parameter, List<string> listparam)
        {
            List<HoadonthuebaotrasauDTO> listtb_hoadontrasau = new List<HoadonthuebaotrasauDTO>();
            DataTable dt = provider.ExcuteQuery("sp_hoadonthuebaotrasau", parameter, listparam);
            if (dt != null && dt.Rows.Count > 0)
            {
                HoadonthuebaotrasauDTO hoadon_tbtrasau;
                foreach (DataRow row in dt.Rows)
                {
                    int iMaHD = int.Parse(row["iMaHD"].ToString());
                    int iMaThueBao = int.Parse(row["iMaThueBao"].ToString());
                    int iMaNV = int.Parse(row["iMaNV"].ToString());
                    int iMaLoai = int.Parse(row["iMaLoai"].ToString());
                    string sTenThueBao = row["sTenThueBao"].ToString();
                    string sSDT = row["sSDT"].ToString();
                    bool bGioitinh = bool.Parse(row["bGioitinh"].ToString());
                    int fThoigiangoi = int.Parse(row["fThoiGianGoi"].ToString());
                    int fTinnhangui = int.Parse(row["iTinNhanGui"].ToString());
                    DateTime dNgaylap = Convert.ToDateTime(row["dNgayLap"].ToString());
                    float fTongTien = float.Parse(row["fTongTien"].ToString());
                    hoadon_tbtrasau = new HoadonthuebaotrasauDTO(iMaHD, iMaThueBao, iMaNV, iMaLoai, sTenThueBao, bGioitinh, sSDT, fTongTien, dNgaylap, fThoigiangoi, fTinnhangui);
                    listtb_hoadontrasau.Add(hoadon_tbtrasau);
                }
            }
            return listtb_hoadontrasau;
        }
        public List<HoadonthuebaotrasauDTO> getdatatketheongay(object[] parameter, List<string> listparam)
        {
            List<HoadonthuebaotrasauDTO> listtb_hoadontrasau = new List<HoadonthuebaotrasauDTO>();
            DataTable dt = provider.ExcuteQuery("sp_thongketheongay", parameter, listparam);
            if (dt != null && dt.Rows.Count > 0)
            {
                HoadonthuebaotrasauDTO hoadon_tbtrasau;
                foreach (DataRow row in dt.Rows)
                {
                    int iMaHD = int.Parse(row["iMaHD"].ToString());
                    int iMaThueBao = int.Parse(row["iMaThueBao"].ToString());
                    int iMaNV = int.Parse(row["iMaNV"].ToString());
                    int iMaLoai = int.Parse(row["iMaLoai"].ToString());
                    string sTenThueBao = row["sTenThueBao"].ToString();
                    string sSDT = row["sSDT"].ToString();
                    bool bGioitinh = bool.Parse(row["bGioitinh"].ToString());
                    int fThoigiangoi = int.Parse(row["fThoiGianGoi"].ToString());
                    int fTinnhangui = int.Parse(row["iTinNhanGui"].ToString());
                    DateTime dNgaylap = Convert.ToDateTime(row["dNgayLap"].ToString());
                    float fTongTien = float.Parse(row["fTongTien"].ToString());
                    hoadon_tbtrasau = new HoadonthuebaotrasauDTO(iMaHD, iMaThueBao, iMaNV, iMaLoai, sTenThueBao, bGioitinh, sSDT, fTongTien, dNgaylap, fThoigiangoi, fTinnhangui);
                    listtb_hoadontrasau.Add(hoadon_tbtrasau);
                }
            }
            return listtb_hoadontrasau;

        }
        public string gettong(object[] parameter, List<string> listparameter)
        {
            string tong = "";
            List<HoadonthuebaotrasauDTO> list_tbtrasau = new List<HoadonthuebaotrasauDTO>();
            DataTable dt = provider.ExcuteQuery("sp_tong", parameter, listparameter);
            if (dt != null && dt.Rows.Count > 0)
            {
                tong = (dt.Rows[0][0].ToString());

            }
            return tong;
        }
    }
}
