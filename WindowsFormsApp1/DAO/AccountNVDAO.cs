﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.DAO
{
    class AccountNVDAO
    {
        DataProvider provider = new DataProvider();
        private static AccountNVDAO instance;
        public static AccountNVDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AccountNVDAO();
                }
                return instance;
            }
        }
        public bool insert(object[] parameter, List<string> listpara)
        {
            int result = provider.ExcuteNoneQuery("sp_themaccount", parameter, listpara);
            if (result != 0)
            {
                return true;

            }
            else
                return false;
        }
    }
}
