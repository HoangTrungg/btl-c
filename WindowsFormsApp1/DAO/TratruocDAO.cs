﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.DTO;

namespace WindowsFormsApp1.DAO
{
    class TratruocDAO
    {
        DataProvider provider = new DataProvider();
        private static TratruocDAO instance;
        public static TratruocDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TratruocDAO();
                }
                return instance;
            }
        }
        public List<TratruocDTO> getdata(object[] parameter, List<string> listparam)
        {
            List<TratruocDTO> listtb_tratruoc = new List<TratruocDTO>();
            DataTable dt = provider.ExcuteQuery("sp_thuebaotratruoc", parameter, listparam);
            if (dt != null && dt.Rows.Count > 0)
            {
                TratruocDTO tbtratruoc;
                foreach (DataRow row in dt.Rows)
                {
                    //public NhanVienDTO(int iMaNV, string sTenNV, bool bGioitinh, DateTime dNgaysinh, DateTime dNgayvaolam)
                    int iMaThueBao = int.Parse(row["iMaThueBao"].ToString());
                    int iMaLoai = int.Parse(row["iMaLoai"].ToString());
                    string sTenThueBao = row["sTenThueBao"].ToString();
                    string sSDT = row["sSDT"].ToString();
                    bool bGioitinh = bool.Parse(row["bGioitinh"].ToString());
                    int iTuoi = int.Parse(row["iTuoi"].ToString());
                    string sCMND = row["sCMND"].ToString();
                    DateTime dDangky = Convert.ToDateTime(row["dNgayDangKy"].ToString());
                    float fTongTien = float.Parse(row["fTongTien"].ToString());
                    tbtratruoc = new TratruocDTO(iMaThueBao,iMaLoai,sTenThueBao,bGioitinh,sSDT,iTuoi,sCMND,dDangky,fTongTien);
                    listtb_tratruoc.Add(tbtratruoc);
                }
            }
            return listtb_tratruoc;

        }
        public List<TratruocDTO> getdatanvtratruoc(object[] parameter, List<string> listparam)
        {
            List<TratruocDTO> listtb_tratruoc = new List<TratruocDTO>();
            DataTable dt = provider.ExcuteQuery("sp_thuebaotrasautruoc", parameter, listparam);
            if (dt != null && dt.Rows.Count > 0)
            {
                TratruocDTO tbtratruoc;
                foreach (DataRow row in dt.Rows)
                {
                    //public NhanVienDTO(int iMaNV, string sTenNV, bool bGioitinh, DateTime dNgaysinh, DateTime dNgayvaolam)
                    int iMaThueBao = int.Parse(row["iMaThueBao"].ToString());
                    int iMaLoai = int.Parse(row["iMaLoai"].ToString());
                    string sTenThueBao = row["sTenThueBao"].ToString();
                    string sSDT = row["sSDT"].ToString();
                    bool bGioitinh = bool.Parse(row["bGioitinh"].ToString());
                    int iTuoi = int.Parse(row["iTuoi"].ToString());
                    string sCMND = row["sCMND"].ToString();
                    DateTime dDangky = Convert.ToDateTime(row["dNgayDangKy"].ToString());
                    tbtratruoc = new TratruocDTO(iMaThueBao, iMaLoai, sTenThueBao, bGioitinh, sSDT, iTuoi, sCMND, dDangky);
                    listtb_tratruoc.Add(tbtratruoc);
                }
            }
            return listtb_tratruoc;

        }
        public List<TratruocDTO> getdatanvtratruochuydv(object[] parameter, List<string> listparam)
        {
            List<TratruocDTO> listtb_tratruoc = new List<TratruocDTO>();
            DataTable dt = provider.ExcuteQuery("sp_thuebaotratruochuydvnv", parameter, listparam);
            if (dt != null && dt.Rows.Count > 0)
            {
                TratruocDTO tbtratruoc;
                foreach (DataRow row in dt.Rows)
                {
                    //public NhanVienDTO(int iMaNV, string sTenNV, bool bGioitinh, DateTime dNgaysinh, DateTime dNgayvaolam)
                    int iMaThueBao = int.Parse(row["iMaThueBao"].ToString());
                    int iMaLoai = int.Parse(row["iMaLoai"].ToString());
                    string sTenThueBao = row["sTenThueBao"].ToString();
                    string sSDT = row["sSDT"].ToString();
                    bool bGioitinh = bool.Parse(row["bGioitinh"].ToString());
                    int iTuoi = int.Parse(row["iTuoi"].ToString());
                    string sCMND = row["sCMND"].ToString();
                    DateTime dDangky = Convert.ToDateTime(row["dNgayDangKy"].ToString());
                    tbtratruoc = new TratruocDTO(iMaThueBao, iMaLoai, sTenThueBao, bGioitinh, sSDT, iTuoi, sCMND, dDangky);
                    listtb_tratruoc.Add(tbtratruoc);
                }
            }
            return listtb_tratruoc;

        }
        public bool insert(object[] parameter, List<string> listpara)
        {
            int result = provider.ExcuteNoneQuery("sp_themthuebaotratruoc", parameter, listpara);
            if (result != 0)
            {
                return true;

            }
            else
                return false;
        }
        public bool update(object[] parameter, List<string> listpara)
        {
            int result = provider.ExcuteNoneQuery("sp_updatethuebaotrasau", parameter, listpara);
            if (result != 0)
            {
                return true;
            }
            else
                return false;
        }
    }
}
