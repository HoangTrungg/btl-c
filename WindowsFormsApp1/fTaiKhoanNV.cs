﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DAO;

namespace WindowsFormsApp1
{
    public partial class fTaiKhoanNV : Form
    {
        public fTaiKhoanNV()
        {
            InitializeComponent();
            loadNvDetail();
            btnUpdate.Enabled = false;
        }
        DataProvider dtp = new DataProvider();
        private Boolean flag = false;
        ErrorProvider ErrorProvider = new ErrorProvider();
        private void loadNvDetail()
        {
            string id = "";
            using (StreamReader sr = File.OpenText(fDangnhap.tmpfile))
            {
                id = sr.ReadLine();
            }
            DataTable dt = dtp.ExcuteQuery("sp_getNvById", new object[] { id }, new List<string> { "@id" });
            if (dt != null && dt.Rows.Count > 0)
            {
                txtNvCode.Text = dt.Rows[0]["iMaNv"].ToString();
                txtNvName.Text = dt.Rows[0]["sTenNV"].ToString();
                txtNvUsername.Text = dt.Rows[0]["sUserName"].ToString();
                txtNvRole.Text = (dt.Rows[0]["bLoaiAcc"].ToString() == "0" ? "Admin" : "Nhân Viên");
            }
            else
            {
                MessageBox.Show("Lấy Thông Tin Không Thành Công");
            }
        }

        private void btnCancelAddAccount_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Hủy việc thêm tài khoản ?", "Hủy", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
            {
                Clear();
                this.Close();
            }
        }
        private void Clear()
        {
            txtOldPass.Text = "";
            txtNewPass.Text = "";
            txtRePass.Text = "";
        }
        private void tggPass_CheckedChanged(object sender, EventArgs e)
        {
            lblOldPass.Visible = lblOldPass.Visible == true ? (false) : (true);
            lblNewPass.Visible = lblNewPass.Visible == true ? (false) : (true);
            lblRePass.Visible = lblRePass.Visible == true ? (false) : (true);
            txtOldPass.Visible = txtOldPass.Visible == true ? (false) : (true);
            txtNewPass.Visible = txtNewPass.Visible == true ? (false) : (true);
            txtRePass.Visible = txtRePass.Visible == true ? (false) : (true);
            btnUpdate.Visible = btnUpdate.Visible == true ? (false) : (true);
            Clear();
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (flag == false)
            {
                MessageBox.Show("Kiểm tra lại thông tin");
            }
            else
            {
                string oldPass = txtOldPass.Text;
                string newPass = txtNewPass.Text;
                string id = "";
                using (StreamReader sr = File.OpenText(fDangnhap.tmpfile))
                {
                    id = sr.ReadLine();
                }
                string pass = bum.Hash(newPass);
                string opass = bum.Hash(oldPass);
                int result = dtp.ExcuteNoneQuery("sp_resetPassword", new object[] { id, pass, opass }, new List<string> { "@id", "@pass", "@oldpass" });
                if (result == 0)
                {
                    MessageBox.Show("Đổi Mật Khẩu Không Thành Công");
                }
                else
                {
                    MessageBox.Show("Đổi Mật Khẩu Thành Công, Chuyển Về Trang Đăng Nhập");
                    fDangnhap.DeleteTmpFile(fDangnhap.tmpfile);
                    fDangnhap.nv.Close();
                    this.Close();
                }
            }
        }

        private void txtRePass_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtRePass.Text.Equals(txtNewPass.Text) == false)
            {
                ErrorProvider.SetError(txtRePass, "Mật Khẩu Không Trùng Khớp");
                flag = false;
                btnUpdate.Enabled = false;
            }
            else
            {
                ErrorProvider.Clear();
                btnUpdate.Enabled = true;
                flag = true;
            }
        }
    }
}
