﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WindowsFormsApp1.DAO;
using WindowsFormsApp1.DTO;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class fNhanvien : Form
    {
        public fNhanvien()
        {
            InitializeComponent();
            loadlisttratruoc();
            loadlisttrasau();
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Normal;
            StartPosition = FormStartPosition.CenterScreen;
        }
        List<HoadonthuebaotratruocDTO> list_hoadontratruoc;
        string tmpfile = Path.GetTempPath();


        private void loadlisttratruoc()
        {
            lv_hdthuebaotratruoc.Items.Clear();
            list_hoadontratruoc = HoadonthuebaotratruocDAO.Instance.getdata(null, null);
            ListViewItem item;
            foreach (HoadonthuebaotratruocDTO hdtratruoc in list_hoadontratruoc)
            {
                //int iMaHD, int iMathuebao, int iMaNV,int iMaLoai, string sTenThueBao, bool bGioitinh,
                //string sSDT, float ftongtien, DateTime dNgaylap, int iThoigian, int iTinnhangui
                item = new ListViewItem();
                item.Text = hdtratruoc.IMaHD.ToString();
                item.SubItems.Add(hdtratruoc.IMathuebao.ToString());
                item.SubItems.Add(hdtratruoc.IMaNV.ToString());
                item.SubItems.Add(hdtratruoc.IMaLoai.ToString());
                item.SubItems.Add(hdtratruoc.STenThueBao.ToString());
                item.SubItems.Add(hdtratruoc.BGioitinh == true ? "Nu" : "Nam");
                item.SubItems.Add(hdtratruoc.SSDT.ToString());
                item.SubItems.Add(hdtratruoc.FTongtien.ToString());
                item.SubItems.Add(hdtratruoc.DNgaylap.ToString("dd/MM/yyyy"));
                item.SubItems.Add(hdtratruoc.Ithoigian.ToString());
                item.SubItems.Add(hdtratruoc.ITinnhangui.ToString());
                lv_hdthuebaotratruoc.Items.Add(item);
            }
        }
        List<HoadonthuebaotrasauDTO> list_hoadontrasau;
        private void loadlisttrasau()
        {
            lv_thuebaotrasau.Items.Clear();
            list_hoadontrasau = HoadonthuebaotrasauDAO.Instance.getdata(null, null);
            ListViewItem item;
            foreach (HoadonthuebaotrasauDTO hdtrasau in list_hoadontrasau)
            {
                //int iMaHD, int iMathuebao, int iMaNV,int iMaLoai, string sTenThueBao, bool bGioitinh,
                //string sSDT, float ftongtien, DateTime dNgaylap, int iThoigian, int iTinnhangui
                item = new ListViewItem();
                item.Text = hdtrasau.IMaHD.ToString();
                item.SubItems.Add(hdtrasau.IMathuebao.ToString());
                item.SubItems.Add(hdtrasau.IMaNV.ToString());
                item.SubItems.Add(hdtrasau.IMaLoai.ToString());
                item.SubItems.Add(hdtrasau.STenThueBao.ToString());
                item.SubItems.Add(hdtrasau.BGioitinh == true ? "Nu" : "Nam");
                item.SubItems.Add(hdtrasau.SSDT.ToString());
                item.SubItems.Add(hdtrasau.FTongtien.ToString());
                item.SubItems.Add(hdtrasau.DNgaylap.ToString("dd/MM/yyyy"));
                item.SubItems.Add(hdtrasau.Ithoigian.ToString());
                item.SubItems.Add(hdtrasau.ITinnhangui.ToString());
                lv_thuebaotrasau.Items.Add(item);
            }
        }
        private void ThôngTinTàiKhoảnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fTaiKhoanNV ftk_nv = new fTaiKhoanNV();
            ftk_nv.ShowDialog();
            this.Show();
        }

        private void QuảnLýThuêBaoTrảSauToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fThuebaotrasau tb_trasau = new fThuebaotrasau();
            this.Hide();
            tb_trasau.ShowDialog();
            this.Show();
        }

        private void QuảnLýThuêBaoTrảTrướcToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fThuebaotratruoc tb_tratruoc = new fThuebaotratruoc();
            this.Hide();
            tb_tratruoc.ShowDialog();
            this.Show();
        }

        private void đăngXuấtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn Có Muốn Đăng Xuất ?", "Đăng Xuất", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
            {
                fDangnhap.DeleteTmpFile(fDangnhap.tmpfile);
                this.Close();
            }
        }

        private void ThuêBaoĐãNgừngSửDụngDịchVụToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fthuebaongungdichvu ftk_nv = new fthuebaongungdichvu();
            this.Hide();
            ftk_nv.ShowDialog();
            this.Show();
        }

        private void fNhanvien_VisibleChanged(object sender, EventArgs e)
        {
            loadlisttratruoc();
            loadlisttrasau();
        }

        private void btnPrintTB_Click(object sender, EventArgs e)
        {
            CrystalReport.fRepor2 fRepor2 = new CrystalReport.fRepor2();
            fRepor2.ShowDialog();
            this.Show();
        }

        private void btnPrintTB2_Click(object sender, EventArgs e)
        {
            
            CrystalRepoert.fRepor freport = new CrystalRepoert.fRepor();
            freport.ShowDialog();
            this.Show();
        }
    }
}
