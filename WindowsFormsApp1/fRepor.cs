﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DAO;
using WindowsFormsApp1.DTO;

namespace WindowsFormsApp1.CrystalRepoert
{
    public partial class fRepor : Form
    {
        public fRepor()
        {
            InitializeComponent();
        }

        private void fRepor_Load(object sender, EventArgs e)
        {
            CrystalReport1 report = new CrystalReport1();
            string query = "SELECT dbo.tblHoaDon.*,dbo.tblThueBao.sTenThueBao,dbo.tblThueBao.bGioitinh,dbo.tblThueBao.sSDT,dbo.tblThueBao.iMaLoai FROM dbo.tblHoaDon,dbo.tblThueBao WHERE dbo.tblHoaDon.iMaThueBao = dbo.tblThueBao.iMaThueBao AND dbo.tblThueBao.iMaLoai = 1";
            report.SetDataSource(new DataProvider().gettable(query));
            fcrystalreport.ReportSource = report;
        }


        
    }
}
