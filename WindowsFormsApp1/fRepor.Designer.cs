﻿namespace WindowsFormsApp1.CrystalRepoert
{
    partial class fRepor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fcrystalreport = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // fcrystalreport
            // 
            this.fcrystalreport.ActiveViewIndex = -1;
            this.fcrystalreport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fcrystalreport.Cursor = System.Windows.Forms.Cursors.Default;
            this.fcrystalreport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fcrystalreport.Location = new System.Drawing.Point(0, 0);
            this.fcrystalreport.Name = "fcrystalreport";
            this.fcrystalreport.Size = new System.Drawing.Size(1320, 568);
            this.fcrystalreport.TabIndex = 0;
            this.fcrystalreport.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // fRepor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1320, 568);
            this.Controls.Add(this.fcrystalreport);
            this.Name = "fRepor";
            this.Text = "fRepor";
            this.Load += new System.EventHandler(this.fRepor_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer fcrystalreport;
    }
}