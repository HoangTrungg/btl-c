﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DAO;

namespace WindowsFormsApp1
{
    public partial class fthemnv : Form
    {
        public fthemnv()
        {
            InitializeComponent();
        }
        ErrorProvider errorprovider = new ErrorProvider();
        private void Btn_them_Click(object sender, EventArgs e)
        {
            if (valid() == false)
            {
                MessageBox.Show("Hãy Kiểm Tra Lại Thông Tin");
                return;
            }
            else
            {
                List<String> listpara = new List<string> { "@sTenNV", "@bGioitinh", "@dNgaysinh", "@dNgayvaolam" };
                string sTenNV = txt_tennv.Text.Trim();
                bool bGT = rd_nam.Checked ? false : true;
                DateTime dNgaysinh = Convert.ToDateTime(dt_ngaysinh.Value.ToString());
                DateTime dNgayvaolam = Convert.ToDateTime(dt_ngayvaolam.Value.ToString());
                object[] parameter = new object[] { sTenNV, bGT, dNgaysinh, dNgayvaolam };
                if (NhanvienDAO.Instance.insert(parameter, listpara))
                {
                    MessageBox.Show("Them thanh cong !!");
                    fAdmin fAdmin = new fAdmin();
                    this.Hide();
                    fAdmin.ShowDialog();
                    this.Show();
                }
                else
                {
                    MessageBox.Show("Loi!");
                }
            }


        }

        private void btnCancelAddAccount_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Hủy việc thêm nhân viên ?", "Hủy", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
            {
                Clear();
                this.Close();
            }
        }
        private void Clear()
        {
            txt_tennv.Text = "";
        }
        private Boolean valid()
        {
            
            if(dt_ngaysinh.Value.Year - DateTime.Now.Year < 18)
            {
                errorprovider.SetError(dt_ngaysinh, "Nhân viên phải lớn hơn 18 tuổi");
                return false;
            }
            else
            {
                errorprovider.Clear();
            }
            if (txt_tennv.Text == "")
            {
                return false;
            }
            return true;
        }
    }
}
