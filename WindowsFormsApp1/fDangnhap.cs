﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Windows.Forms;
using WindowsFormsApp1.DAO;
namespace WindowsFormsApp1
{
    public partial class fDangnhap : Form
    {
        public fDangnhap()
        {
            InitializeComponent();
        }
        DataProvider dtp = new DataProvider();
        ErrorProvider errorProvider = new ErrorProvider();
        public static string tmpfile = Path.GetTempFileName();
        public static fAdmin admin;
        public static fNhanvien nv;



        /*private void FDangnhap_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(MessageBox.Show("Bạn có muốn thoát ?", "Thoát", MessageBoxButtons.OKCancel,MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.OK)
            {
                e.Cancel = true;
            }
        }*/

        private void Btn_dangnhap_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrWhiteSpace(txt_taikhoan.Text))
            //if (txt_taikhoan.Text == "")
            {
                txt_taikhoan.Focus();
                errorProvider.SetError(txt_taikhoan, "Hãy Nhập Mật Khẩu");
                return;
                //lb_messagetk.Visible = true;
                //lb_messagetk.Text = "*Hãy điền tài khoản";
            }
            else
            {
                errorProvider.Clear();
                //lb_messagetk.Visible = false;
            }
            if (string.IsNullOrWhiteSpace(txt_matkhau.Text))
            //if (txt_matkhau.Text == "")
            {
                txt_matkhau.Focus();
                errorProvider.SetError(txt_matkhau, "Hãy Nhập Mật Khẩu");
                return;
                //lb_matkhau.Visible = true;
                //lb_matkhau.Text = "*Hãy điền mật khẩu";
            }
            else
            {
                errorProvider.Clear();
                //lb_matkhau.Visible = false;
            }
            string md5pass = bum.Hash(txt_matkhau.Text);
            DataTable dt = dtp.ExcuteQuery("sp_login", new object[] { txt_taikhoan.Text.Trim(), md5pass }, new List<string> { "@username", "@password" });
            if (dt != null && dt.Rows.Count > 0)
            {
                using (StreamWriter sw = File.CreateText(tmpfile))
                {
                    sw.WriteLine(dt.Rows[0]["iID_NV"].ToString());
                };
                string loaiacc = dt.Rows[0]["bLoaiAcc"].ToString();
                string trangthai = dt.Rows[0]["bTrangthai"].ToString();
                if (trangthai.Equals("True"))
                {
                    MessageBox.Show("Tài Khoản Này Đã Ngừng Hoạt Động");
                    return;
                }
                if (loaiacc.Equals("False") && trangthai.Equals("False"))
                {
                    admin = new fAdmin();
                    txt_taikhoan.Focus();
                    this.Clear();
                    this.Hide();
                    admin.ShowDialog();
                    this.Show();
                }
                if (loaiacc.Equals("True") && trangthai.Equals("False"))
                {
                    nv = new fNhanvien();
                    txt_taikhoan.Focus();
                    this.Clear();
                    this.Hide();
                    nv.ShowDialog();
                    this.Show();
                }
                
            }
            else
            {
                MessageBox.Show("Đăng Nhập Không Thành Công");
            }
        }

        private void formExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn thoát ?", "Thoát", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
            {
                DeleteTmpFile(tmpfile);
                Application.Exit();
            }
        }

        private void txt_matkhau_KeyUp(object sender, KeyEventArgs e)
        {
            EventArgs ev = new EventArgs();
            if (e.KeyCode == Keys.Enter)
            {
                Btn_dangnhap_Click(sender, ev);
            }
        }
        public static void DeleteTmpFile(string tmp)
        {
            try
            {
                // Delete the temp file (if it exists)
                if (File.Exists(tmp))
                {
                    File.Delete(tmp);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error deleteing TEMP file: " + ex.Message);
            }
        }
        private void Clear()
        {
            txt_matkhau.Text = "";
            txt_taikhoan.Text = "";
        }
    }
}
