﻿namespace WindowsFormsApp1
{
    partial class fThuebaotratruoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_huy = new System.Windows.Forms.Button();
            this.txt_sdt = new System.Windows.Forms.TextBox();
            this.txt_tuoi = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txt_tenthuebao = new System.Windows.Forms.TextBox();
            this.txt_cmnd = new System.Windows.Forms.TextBox();
            this.txt_loaithuebao = new System.Windows.Forms.TextBox();
            this.txt_mathuebao = new System.Windows.Forms.TextBox();
            this.btn_sua = new System.Windows.Forms.Button();
            this.rd_nam = new System.Windows.Forms.RadioButton();
            this.label28 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.rd_nu = new System.Windows.Forms.RadioButton();
            this.label34 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.btn_them = new System.Windows.Forms.Button();
            this.dt_pickngaydangky = new System.Windows.Forms.DateTimePicker();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lv_tratruoc = new System.Windows.Forms.ListView();
            this.iMaThueBao = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.iMaLoai = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sTenThueBao = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Gioitinh = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sSDT = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.iTuoi = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sCMND = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dNgayDangKy = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.fTongTien = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_huy
            // 
            this.btn_huy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_huy.BackColor = System.Drawing.Color.OrangeRed;
            this.btn_huy.Enabled = false;
            this.btn_huy.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.btn_huy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_huy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.btn_huy.ForeColor = System.Drawing.Color.White;
            this.btn_huy.Location = new System.Drawing.Point(861, 448);
            this.btn_huy.Margin = new System.Windows.Forms.Padding(2);
            this.btn_huy.Name = "btn_huy";
            this.btn_huy.Size = new System.Drawing.Size(128, 41);
            this.btn_huy.TabIndex = 12;
            this.btn_huy.Text = "Ngưng Dịch Vụ";
            this.btn_huy.UseVisualStyleBackColor = false;
            this.btn_huy.Click += new System.EventHandler(this.Btn_huy_Click);
            // 
            // txt_sdt
            // 
            this.txt_sdt.Enabled = false;
            this.txt_sdt.Location = new System.Drawing.Point(593, 255);
            this.txt_sdt.Margin = new System.Windows.Forms.Padding(2);
            this.txt_sdt.Name = "txt_sdt";
            this.txt_sdt.Size = new System.Drawing.Size(236, 20);
            this.txt_sdt.TabIndex = 6;
            this.txt_sdt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_sdt_KeyPress);
            // 
            // txt_tuoi
            // 
            this.txt_tuoi.Enabled = false;
            this.txt_tuoi.Location = new System.Drawing.Point(593, 336);
            this.txt_tuoi.Margin = new System.Windows.Forms.Padding(2);
            this.txt_tuoi.Name = "txt_tuoi";
            this.txt_tuoi.Size = new System.Drawing.Size(236, 20);
            this.txt_tuoi.TabIndex = 8;
            this.txt_tuoi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_tuoi_KeyPress);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label36.Location = new System.Drawing.Point(402, 9);
            this.label36.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(191, 25);
            this.label36.TabIndex = 8;
            this.label36.Text = "Thuê bao trả trước";
            // 
            // txt_tenthuebao
            // 
            this.txt_tenthuebao.Enabled = false;
            this.txt_tenthuebao.Location = new System.Drawing.Point(116, 336);
            this.txt_tenthuebao.Margin = new System.Windows.Forms.Padding(2);
            this.txt_tenthuebao.Name = "txt_tenthuebao";
            this.txt_tenthuebao.Size = new System.Drawing.Size(236, 20);
            this.txt_tenthuebao.TabIndex = 3;
            // 
            // txt_cmnd
            // 
            this.txt_cmnd.Enabled = false;
            this.txt_cmnd.Location = new System.Drawing.Point(593, 296);
            this.txt_cmnd.Margin = new System.Windows.Forms.Padding(2);
            this.txt_cmnd.Name = "txt_cmnd";
            this.txt_cmnd.Size = new System.Drawing.Size(236, 20);
            this.txt_cmnd.TabIndex = 7;
            this.txt_cmnd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_cmnd_KeyPress);
            // 
            // txt_loaithuebao
            // 
            this.txt_loaithuebao.Enabled = false;
            this.txt_loaithuebao.Location = new System.Drawing.Point(116, 294);
            this.txt_loaithuebao.Margin = new System.Windows.Forms.Padding(2);
            this.txt_loaithuebao.Name = "txt_loaithuebao";
            this.txt_loaithuebao.Size = new System.Drawing.Size(236, 20);
            this.txt_loaithuebao.TabIndex = 2;
            // 
            // txt_mathuebao
            // 
            this.txt_mathuebao.Enabled = false;
            this.txt_mathuebao.Location = new System.Drawing.Point(116, 255);
            this.txt_mathuebao.Margin = new System.Windows.Forms.Padding(2);
            this.txt_mathuebao.Name = "txt_mathuebao";
            this.txt_mathuebao.Size = new System.Drawing.Size(236, 20);
            this.txt_mathuebao.TabIndex = 1;
            // 
            // btn_sua
            // 
            this.btn_sua.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_sua.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.btn_sua.Enabled = false;
            this.btn_sua.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LimeGreen;
            this.btn_sua.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_sua.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.btn_sua.ForeColor = System.Drawing.Color.White;
            this.btn_sua.Location = new System.Drawing.Point(777, 448);
            this.btn_sua.Margin = new System.Windows.Forms.Padding(2);
            this.btn_sua.Name = "btn_sua";
            this.btn_sua.Size = new System.Drawing.Size(80, 41);
            this.btn_sua.TabIndex = 11;
            this.btn_sua.Text = "Sửa";
            this.btn_sua.UseVisualStyleBackColor = false;
            this.btn_sua.Click += new System.EventHandler(this.Btn_sua_Click);
            // 
            // rd_nam
            // 
            this.rd_nam.AutoSize = true;
            this.rd_nam.Checked = true;
            this.rd_nam.Enabled = false;
            this.rd_nam.Location = new System.Drawing.Point(116, 375);
            this.rd_nam.Margin = new System.Windows.Forms.Padding(2);
            this.rd_nam.Name = "rd_nam";
            this.rd_nam.Size = new System.Drawing.Size(47, 17);
            this.rd_nam.TabIndex = 4;
            this.rd_nam.TabStop = true;
            this.rd_nam.Text = "Nam";
            this.rd_nam.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(161, 297);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(28, 13);
            this.label28.TabIndex = 14;
            this.label28.Text = "Tuổi";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(-62, 218);
            this.label35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(67, 13);
            this.label35.TabIndex = 15;
            this.label35.Text = "Mã thuê bao";
            // 
            // rd_nu
            // 
            this.rd_nu.AutoSize = true;
            this.rd_nu.Enabled = false;
            this.rd_nu.Location = new System.Drawing.Point(207, 375);
            this.rd_nu.Margin = new System.Windows.Forms.Padding(2);
            this.rd_nu.Name = "rd_nu";
            this.rd_nu.Size = new System.Drawing.Size(39, 17);
            this.rd_nu.TabIndex = 5;
            this.rd_nu.Text = "Nữ";
            this.rd_nu.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(40, 258);
            this.label34.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(67, 13);
            this.label34.TabIndex = 17;
            this.label34.Text = "Mã thuê bao";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(40, 299);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(72, 13);
            this.label30.TabIndex = 12;
            this.label30.Text = "Loại thuê bao";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(505, 377);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(74, 13);
            this.label31.TabIndex = 11;
            this.label31.Text = "Ngày đăng ký";
            // 
            // btn_them
            // 
            this.btn_them.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_them.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_them.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btn_them.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_them.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.btn_them.ForeColor = System.Drawing.Color.White;
            this.btn_them.Location = new System.Drawing.Point(693, 448);
            this.btn_them.Margin = new System.Windows.Forms.Padding(2);
            this.btn_them.Name = "btn_them";
            this.btn_them.Size = new System.Drawing.Size(80, 41);
            this.btn_them.TabIndex = 10;
            this.btn_them.Text = "Thêm";
            this.btn_them.UseVisualStyleBackColor = false;
            this.btn_them.Click += new System.EventHandler(this.Button10_Click);
            // 
            // dt_pickngaydangky
            // 
            this.dt_pickngaydangky.Enabled = false;
            this.dt_pickngaydangky.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_pickngaydangky.Location = new System.Drawing.Point(593, 375);
            this.dt_pickngaydangky.Margin = new System.Windows.Forms.Padding(2);
            this.dt_pickngaydangky.Name = "dt_pickngaydangky";
            this.dt_pickngaydangky.Size = new System.Drawing.Size(236, 20);
            this.dt_pickngaydangky.TabIndex = 9;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(505, 299);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(39, 13);
            this.label33.TabIndex = 10;
            this.label33.Text = "CMND";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 339);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Tên thuê bao";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(505, 258);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Số điện thoại";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(505, 339);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Tuổi";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 377);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Giới tính";
            // 
            // lv_tratruoc
            // 
            this.lv_tratruoc.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.iMaThueBao,
            this.iMaLoai,
            this.sTenThueBao,
            this.Gioitinh,
            this.sSDT,
            this.iTuoi,
            this.sCMND,
            this.dNgayDangKy,
            this.fTongTien});
            this.lv_tratruoc.FullRowSelect = true;
            this.lv_tratruoc.Location = new System.Drawing.Point(9, 54);
            this.lv_tratruoc.Margin = new System.Windows.Forms.Padding(2);
            this.lv_tratruoc.Name = "lv_tratruoc";
            this.lv_tratruoc.Size = new System.Drawing.Size(980, 177);
            this.lv_tratruoc.TabIndex = 30;
            this.lv_tratruoc.UseCompatibleStateImageBehavior = false;
            this.lv_tratruoc.View = System.Windows.Forms.View.Details;
            this.lv_tratruoc.SelectedIndexChanged += new System.EventHandler(this.Lv_tratruoc_SelectedIndexChanged);
            // 
            // iMaThueBao
            // 
            this.iMaThueBao.Text = "Mã thuê bao";
            this.iMaThueBao.Width = 88;
            // 
            // iMaLoai
            // 
            this.iMaLoai.Text = "Loại thuê bao";
            this.iMaLoai.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iMaLoai.Width = 128;
            // 
            // sTenThueBao
            // 
            this.sTenThueBao.Text = "Tên";
            this.sTenThueBao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sTenThueBao.Width = 159;
            // 
            // Gioitinh
            // 
            this.Gioitinh.DisplayIndex = 4;
            this.Gioitinh.Text = "Giới tính";
            this.Gioitinh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // sSDT
            // 
            this.sSDT.DisplayIndex = 3;
            this.sSDT.Text = "SDT";
            this.sSDT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sSDT.Width = 110;
            // 
            // iTuoi
            // 
            this.iTuoi.Text = "Tuổi";
            this.iTuoi.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iTuoi.Width = 74;
            // 
            // sCMND
            // 
            this.sCMND.Text = "CMND";
            this.sCMND.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sCMND.Width = 100;
            // 
            // dNgayDangKy
            // 
            this.dNgayDangKy.Text = "Ngày đăng ký";
            this.dNgayDangKy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dNgayDangKy.Width = 111;
            // 
            // fTongTien
            // 
            this.fTongTien.Text = "Tổng tiền";
            this.fTongTien.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.fTongTien.Width = 143;
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBack.BackColor = System.Drawing.Color.Red;
            this.btnBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkRed;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(11, 448);
            this.btnBack.Margin = new System.Windows.Forms.Padding(2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(78, 41);
            this.btnBack.TabIndex = 13;
            this.btnBack.Text = "Trở Về";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // fThuebaotratruoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 500);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.lv_tratruoc);
            this.Controls.Add(this.btn_huy);
            this.Controls.Add(this.txt_sdt);
            this.Controls.Add(this.txt_tuoi);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.txt_tenthuebao);
            this.Controls.Add(this.txt_cmnd);
            this.Controls.Add(this.txt_loaithuebao);
            this.Controls.Add(this.txt_mathuebao);
            this.Controls.Add(this.btn_sua);
            this.Controls.Add(this.rd_nam);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.rd_nu);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.btn_them);
            this.Controls.Add(this.dt_pickngaydangky);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label33);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "fThuebaotratruoc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fThuebaotratruoc";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_huy;
        private System.Windows.Forms.TextBox txt_sdt;
        private System.Windows.Forms.TextBox txt_tuoi;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txt_tenthuebao;
        private System.Windows.Forms.TextBox txt_cmnd;
        private System.Windows.Forms.TextBox txt_loaithuebao;
        private System.Windows.Forms.TextBox txt_mathuebao;
        private System.Windows.Forms.Button btn_sua;
        private System.Windows.Forms.RadioButton rd_nam;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.RadioButton rd_nu;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button btn_them;
        private System.Windows.Forms.DateTimePicker dt_pickngaydangky;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListView lv_tratruoc;
        private System.Windows.Forms.ColumnHeader iMaThueBao;
        private System.Windows.Forms.ColumnHeader iMaLoai;
        private System.Windows.Forms.ColumnHeader sTenThueBao;
        private System.Windows.Forms.ColumnHeader Gioitinh;
        private System.Windows.Forms.ColumnHeader sSDT;
        private System.Windows.Forms.ColumnHeader iTuoi;
        private System.Windows.Forms.ColumnHeader sCMND;
        private System.Windows.Forms.ColumnHeader dNgayDangKy;
        private System.Windows.Forms.ColumnHeader fTongTien;
        private System.Windows.Forms.Button btnBack;
    }
}