﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.DTO
{
    class HoaDonDTO
    {
        int iMaHD, iMathuebao, iMaNV;
        float fTongtien;
        DateTime dNgaylap;
        int ithoigian, iTinnhangui;
        public HoaDonDTO()
        {

        }
        public HoaDonDTO(int iMaHD, int iMathuebao, int iMaNV, float ftongtien, DateTime dNgaylap, int iThoigian, int iTinnhangui)
        {
            this.IMaHD = iMaHD;
            this.IMathuebao = iMathuebao;
            this.IMaNV = iMaNV;
            this.FTongtien = ftongtien;
            this.DNgaylap = dNgaylap;
            this.Ithoigian = iThoigian;
            this.iTinnhangui = iTinnhangui;
        }

        public int IMaHD { get => iMaHD; set => iMaHD = value; }
        public int IMathuebao { get => iMathuebao; set => iMathuebao = value; }
        public int IMaNV { get => iMaNV; set => iMaNV = value; }
        public float FTongtien { get => fTongtien; set => fTongtien = value; }
        public DateTime DNgaylap { get => dNgaylap; set => dNgaylap = value; }
        public int Ithoigian { get => Ithoigian1; set => Ithoigian1 = value; }
        public int Ithoigian1 { get => ithoigian; set => ithoigian = value; }
    }
}
