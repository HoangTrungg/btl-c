﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.DTO
{
    class NhanVienDTO
    {
        int iMaNV;
        string sTenNV;
        bool bGioitinh;
        DateTime dNgaysinh, dNgayvaolam;
        public NhanVienDTO()
        {

        }
        public NhanVienDTO(int iMaNV, string sTenNV, bool bGioitinh, DateTime dNgaysinh, DateTime dNgayvaolam)
        {
            this.IMaNV = iMaNV;
            this.STenNV = sTenNV;
            this.BGioitinh = bGioitinh;
            this.DNgaysinh = dNgaysinh;
            this.DNgayvaolam = dNgayvaolam;
        }

        public int IMaNV { get => iMaNV; set => iMaNV = value; }
        public bool BGioitinh { get => bGioitinh; set => bGioitinh = value; }
        public DateTime DNgaysinh { get => dNgaysinh; set => dNgaysinh = value; }
        public DateTime DNgayvaolam { get => dNgayvaolam; set => dNgayvaolam = value; }
        public string STenNV { get => sTenNV; set => sTenNV = value; }
    }
}
