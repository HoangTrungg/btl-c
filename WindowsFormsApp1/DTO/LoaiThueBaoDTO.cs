﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.DTO
{
    class LoaiThueBaoDTO
    {
        int iMaloaiTb;
        string sLoaithuebao;
        public LoaiThueBaoDTO()
        {

        }
        public LoaiThueBaoDTO(int iMaloaiTb, string sLoaithuebao)
        {
            this.iMaloaiTb = iMaloaiTb;
            this.sLoaithuebao = sLoaithuebao;
        }
        public int IMaloaiTb
        {
            get { return iMaloaiTb; }
            set { iMaloaiTb = value; }
        }
        public string SLoaithuebao
        {
            get { return sLoaithuebao; }
            set { sLoaithuebao = value; }
        }
    }
}
