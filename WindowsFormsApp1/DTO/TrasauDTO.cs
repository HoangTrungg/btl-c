﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.DTO
{
    class TrasauDTO
    {
        int iMathuebao, iMaloai, iTuoi;
        string sTenthuebao, sSDT, sCMND;
        DateTime dNgaydangky;
        bool bgioitinh;
        float fTongTien;

        public TrasauDTO() { }

      

        public TrasauDTO(int iMaThueBao, int iMaLoai, string sTenThueBao, bool bGioitinh,string sSDT, int iTuoi, string sCMND, DateTime dDangky, float fTongTien)
        {
            iMathuebao = iMaThueBao;
            iMaloai = iMaLoai;
            sTenthuebao = sTenThueBao;
            this.sSDT = sSDT;
            this.bGioitinh = bGioitinh;
            this.iTuoi = iTuoi;
            this.sCMND = sCMND;
            this.dNgaydangky = dDangky;
            this.fTongTien = fTongTien;
        }

        
        public int IMathuebao
        {
            get { return iMathuebao; }
            set { iMathuebao = value; }
        }
        public int IMaloai
        {
            get { return iMaloai; }
            set { iMaloai = value; }
        }
        public int ITuoi
        {
            get { return iTuoi; }
            set { iTuoi = value; }
        }
        public string STenthuebao
        {
            get { return sTenthuebao; }
            set { sTenthuebao = value; }
        }
        public string SSDT
        {
            get { return sSDT; }
            set { sSDT = value; }
        }
        public string SCMND
        {
            get { return sCMND; }
            set { sCMND = value; }
        }
        public DateTime DNgaydangky
        {
            get { return dNgaydangky; }
            set { dNgaydangky = value; }
        }
        public bool bGioitinh
        {
            get { return bgioitinh; }
            set { bgioitinh = value; }
        }

        public float FTongTien { get => fTongTien; set => fTongTien = value; }
    }
}

