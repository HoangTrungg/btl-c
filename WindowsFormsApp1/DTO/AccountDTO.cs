﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.DTO
{
    class AccountDTO
    {
        string sUserName, sPassword;
        int iIDNV;
        bool bLoaiacc;
        public AccountDTO()
        {

        }
        public AccountDTO(string sUserName, string sPassword, int iIDNV, bool bLoaiacc)
        {
            this.SUserName = sUserName;
            this.SPassword = sPassword;
            this.IIDNV = iIDNV;
            this.BLoaiacc = bLoaiacc;
        }

        public string SUserName { get => sUserName; set => sUserName = value; }
        public string SPassword { get => sPassword; set => sPassword = value; }
        public int IIDNV { get => iIDNV; set => iIDNV = value; }
        public bool BLoaiacc { get => bLoaiacc; set => bLoaiacc = value; }
    }
}
