﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.DTO
{
    class HoadonthuebaotratruocDTO
    {
        int iMaHD, iMathuebao, iMaNV, iMaLoai;
        float fTongtien;
        DateTime dNgaylap;
        int ithoigian, iTinnhangui;
        string sTenThueBao, sSDT;
        bool bGioitinh;
        private float tongtien;

        public HoadonthuebaotratruocDTO()
        {

        }

        public HoadonthuebaotratruocDTO(float tongtien)
        {
            this.tongtien = tongtien;
        }

        public HoadonthuebaotratruocDTO(int iMaHD, int iMathuebao, int iMaNV,int iMaLoai, string sTenThueBao, bool bGioitinh, string sSDT, float ftongtien, DateTime dNgaylap, int iThoigian, int iTinnhangui)
        {
            this.IMaHD = iMaHD;
            this.IMathuebao = iMathuebao;
            this.IMaNV = iMaNV;
            this.FTongtien = ftongtien;
            this.DNgaylap = dNgaylap;
            this.Ithoigian = iThoigian;
            this.iTinnhangui = iTinnhangui;
            this.IMaLoai = iMaLoai;
            this.STenThueBao = sTenThueBao;
            this.sSDT = sSDT;
            this.BGioitinh = bGioitinh;
        }

        public int IMaHD { get => iMaHD; set => iMaHD = value; }
        public int IMathuebao { get => iMathuebao; set => iMathuebao = value; }
        public int IMaNV { get => iMaNV; set => iMaNV = value; }
        public float FTongtien { get => fTongtien; set => fTongtien = value; }
        public DateTime DNgaylap { get => dNgaylap; set => dNgaylap = value; }
        public int Ithoigian { get => ithoigian; set => ithoigian = value; }
        public int ITinnhangui { get => iTinnhangui; set => iTinnhangui = value; }
        public int IMaLoai { get => iMaLoai; set => iMaLoai = value; }
        public string STenThueBao { get => sTenThueBao; set => sTenThueBao = value; }
        public bool BGioitinh { get => bGioitinh; set => bGioitinh = value; }
        public string SSDT { get => sSDT; set => sSDT = value; }
    }
}
